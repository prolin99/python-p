# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QCheckBox, QLabel,
    QLineEdit, QListWidget, QListWidgetItem, QMainWindow,
    QMenuBar, QPushButton, QSizePolicy, QStatusBar,
    QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(523, 641)
        font = QFont()
        font.setFamilies([u"Arial"])
        font.setPointSize(12)
        MainWindow.setFont(font)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.listWidget = QListWidget(self.centralwidget)
        self.listWidget.setObjectName(u"listWidget")
        self.listWidget.setGeometry(QRect(10, 160, 501, 361))
        self.listWidget.setEditTriggers(QAbstractItemView.DoubleClicked|QAbstractItemView.EditKeyPressed)
        self.listWidget.setDragEnabled(False)
        self.listWidget.setDragDropOverwriteMode(False)
        self.listWidget.setDragDropMode(QAbstractItemView.DropOnly)
        self.listWidget.setDefaultDropAction(Qt.MoveAction)
        self.btn_selectFiles = QPushButton(self.centralwidget)
        self.btn_selectFiles.setObjectName(u"btn_selectFiles")
        self.btn_selectFiles.setGeometry(QRect(10, 20, 81, 30))
        icon = QIcon()
        iconThemeName = u"add"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.btn_selectFiles.setIcon(icon)
        self.Chk_bookMark = QCheckBox(self.centralwidget)
        self.Chk_bookMark.setObjectName(u"Chk_bookMark")
        self.Chk_bookMark.setGeometry(QRect(230, 530, 81, 41))
        self.Chk_bookMark.setChecked(True)
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(220, 20, 111, 31))
        self.txt_beg = QLineEdit(self.centralwidget)
        self.txt_beg.setObjectName(u"txt_beg")
        self.txt_beg.setGeometry(QRect(340, 20, 41, 31))
        self.txtRep = QLineEdit(self.centralwidget)
        self.txtRep.setObjectName(u"txtRep")
        self.txtRep.setGeometry(QRect(90, 70, 81, 31))
        self.Btn_Join = QPushButton(self.centralwidget)
        self.Btn_Join.setObjectName(u"Btn_Join")
        self.Btn_Join.setGeometry(QRect(430, 530, 81, 30))
        icon1 = QIcon()
        iconThemeName = u"document-save"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.Btn_Join.setIcon(icon1)
        self.chkEditMode = QCheckBox(self.centralwidget)
        self.chkEditMode.setObjectName(u"chkEditMode")
        self.chkEditMode.setGeometry(QRect(10, 120, 131, 27))
        self.btn_New = QPushButton(self.centralwidget)
        self.btn_New.setObjectName(u"btn_New")
        self.btn_New.setGeometry(QRect(10, 530, 61, 41))
        icon2 = QIcon()
        iconThemeName = u"dialog-cancel"
        if QIcon.hasThemeIcon(iconThemeName):
            icon2 = QIcon.fromTheme(iconThemeName)
        else:
            icon2.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.btn_New.setIcon(icon2)
        self.btn_remove = QPushButton(self.centralwidget)
        self.btn_remove.setObjectName(u"btn_remove")
        self.btn_remove.setGeometry(QRect(120, 20, 61, 30))
        icon3 = QIcon()
        iconThemeName = u"remove"
        if QIcon.hasThemeIcon(iconThemeName):
            icon3 = QIcon.fromTheme(iconThemeName)
        else:
            icon3.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.btn_remove.setIcon(icon3)
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(10, 70, 71, 31))
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(180, 70, 91, 31))
        self.txt_rep_new = QLineEdit(self.centralwidget)
        self.txt_rep_new.setObjectName(u"txt_rep_new")
        self.txt_rep_new.setGeometry(QRect(250, 70, 113, 29))
        self.btn_replace = QPushButton(self.centralwidget)
        self.btn_replace.setObjectName(u"btn_replace")
        self.btn_replace.setGeometry(QRect(390, 70, 71, 30))
        self.chk_Menu_show = QCheckBox(self.centralwidget)
        self.chk_Menu_show.setObjectName(u"chk_Menu_show")
        self.chk_Menu_show.setGeometry(QRect(170, 120, 141, 27))
        self.chk_doc2pdf = QCheckBox(self.centralwidget)
        self.chk_doc2pdf.setObjectName(u"chk_doc2pdf")
        self.chk_doc2pdf.setGeometry(QRect(80, 540, 111, 21))
        self.chk_doc2pdf.setChecked(True)
        self.btn_cut = QPushButton(self.centralwidget)
        self.btn_cut.setObjectName(u"btn_cut")
        self.btn_cut.setGeometry(QRect(390, 20, 71, 31))
        self.chkcompress = QCheckBox(self.centralwidget)
        self.chkcompress.setObjectName(u"chkcompress")
        self.chkcompress.setEnabled(False)
        self.chkcompress.setGeometry(QRect(340, 540, 54, 21))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 523, 20))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"PDF 合併、目錄建製", None))
#if QT_CONFIG(tooltip)
        self.listWidget.setToolTip(QCoreApplication.translate("MainWindow", u"直接扡移排列順序", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        self.btn_selectFiles.setToolTip(QCoreApplication.translate("MainWindow", u"新增要加入合併的文件檔", None))
#endif // QT_CONFIG(tooltip)
        self.btn_selectFiles.setText(QCoreApplication.translate("MainWindow", u"增檔", None))
#if QT_CONFIG(tooltip)
        self.Chk_bookMark.setToolTip(QCoreApplication.translate("MainWindow", u"合併的 PDF 中加上目錄", None))
#endif // QT_CONFIG(tooltip)
        self.Chk_bookMark.setText(QCoreApplication.translate("MainWindow", u"加目錄", None))
#if QT_CONFIG(tooltip)
        self.label.setToolTip(QCoreApplication.translate("MainWindow", u"匯入檔名時自動去除", None))
#endif // QT_CONFIG(tooltip)
        self.label.setText(QCoreApplication.translate("MainWindow", u"略去開頭字數：", None))
        self.txt_beg.setText("")
#if QT_CONFIG(tooltip)
        self.txtRep.setToolTip(QCoreApplication.translate("MainWindow", u"目錄中的文字替代", None))
#endif // QT_CONFIG(tooltip)
        self.txtRep.setText(QCoreApplication.translate("MainWindow", u"_ok", None))
        self.Btn_Join.setText(QCoreApplication.translate("MainWindow", u"執行合併", None))
#if QT_CONFIG(tooltip)
        self.chkEditMode.setToolTip(QCoreApplication.translate("MainWindow", u"連擊進入修改目錄名稱", None))
#endif // QT_CONFIG(tooltip)
        self.chkEditMode.setText(QCoreApplication.translate("MainWindow", u"編修模式", None))
        self.btn_New.setText(QCoreApplication.translate("MainWindow", u"清空", None))
#if QT_CONFIG(tooltip)
        self.btn_remove.setToolTip(QCoreApplication.translate("MainWindow", u"移除選中的檔案", None))
#endif // QT_CONFIG(tooltip)
        self.btn_remove.setText(QCoreApplication.translate("MainWindow", u"刪檔", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"尋找：", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"取代為：", None))
        self.btn_replace.setText(QCoreApplication.translate("MainWindow", u"置換", None))
#if QT_CONFIG(tooltip)
        self.chk_Menu_show.setToolTip(QCoreApplication.translate("MainWindow", u"是否出現在目錄中", None))
#endif // QT_CONFIG(tooltip)
        self.chk_Menu_show.setText(QCoreApplication.translate("MainWindow", u"出現/隱藏", None))
#if QT_CONFIG(tooltip)
        self.chk_doc2pdf.setToolTip(QCoreApplication.translate("MainWindow", u"保留轉換過程暫存的 pdf ", None))
#endif // QT_CONFIG(tooltip)
        self.chk_doc2pdf.setText(QCoreApplication.translate("MainWindow", u"保留doc2PDF", None))
#if QT_CONFIG(tooltip)
        self.btn_cut.setToolTip(QCoreApplication.translate("MainWindow", u"再把列表中開頭文字去除", None))
#endif // QT_CONFIG(tooltip)
        self.btn_cut.setText(QCoreApplication.translate("MainWindow", u"再裁切", None))
#if QT_CONFIG(tooltip)
        self.chkcompress.setToolTip(QCoreApplication.translate("MainWindow", u"會降低品質", None))
#endif // QT_CONFIG(tooltip)
        self.chkcompress.setText(QCoreApplication.translate("MainWindow", u"壓縮", None))
    # retranslateUi

