import os ,re ,shutil
import PyPDF2
import tempfile
from PIL import Image
from PySide6.QtCore import *
from PySide6.QtWidgets import *

#pip install comtypes
from comtypes.client import CreateObject


def do_join_work(pdfFiles_list , bookmaek_list , menu_show_list , bookmake_fg , conv_save_fg  ,  save_pdf_name , progress_dialog):
    # 合併

    BookMark_page=[0]

    #儲存檔名
    if save_pdf_name.lower().endswith(".pdf"):
        save_pdf_name = save_pdf_name[:-4]
        print(save_pdf_name)

    # 合併成一個 PDF --------------------------------
    print("合併成一個 PDF --------------------------------")
    # 創建新 PDF 文件
    pdfWriter_tmp = PyPDF2.PdfWriter()

    mark_page = 0
    with tempfile.TemporaryDirectory() as tmpdir: # 创建一个临时目录
        wdToPDF = CreateObject("Word.Application") # 建立Word應用物件
        wdFormatPDF = 17 # 設定輸出格式為PDF
        print(tmpdir) # 打印临时目录的路径

        i = 0 
        file_count = len(pdfFiles_list)

        for file in pdfFiles_list:
            if file.lower().endswith((".docx",".doc",".odt")) :
                #使用 word 轉成 pdf ，不可以中文，先複製到暫存區
                if file.lower().endswith(".docx"):
                    newfile = tmpdir + "/tmp_doc.docx"
                    toPdf_file = file[:-5] + ".pdf"
                else:
                    newfile = tmpdir + "/tmp_doc" + file[-4:]
                    toPdf_file = file[:-4] + ".pdf"

                shutil.copyfile(file , newfile ) # 复制文件到新的路径
                docx2pdf_file =tmpdir + "/tmp_doc2pdf.pdf"

                print(newfile)
                doc = wdToPDF.Documents.Open(newfile) # 開啟Word文件
                doc.SaveAs(docx2pdf_file, FileFormat=wdFormatPDF) # 儲存為PDF格式
                doc.Close() # 關閉Word文件
                file = docx2pdf_file

            if file.lower().endswith((".jpg", ".png")):
                newfile = tmpdir + "/tmp_picture" + file[-4:]
                shutil.copyfile(file , newfile ) # 复制文件到新的路径
                docx2pdf_file =tmpdir + "/tmp_doc2pdf.pdf"
                toPdf_file = file[:-4] + ".pdf"
                #圖檔轉成 pdf 縮放在 a4 內
                img = Image.open(newfile)
                rs = 1
                if img.size[0] > 595 :
                    rs = (img.size[0] // 595) +1
                # 縮放圖像，使其寬度和高度都為原來的一半
                new_size = (img.size[0] // rs, img.size[1] // rs)
                resized_image = img.resize(new_size)
                resized_image.convert('RGB').save(docx2pdf_file, 'PDF')
                file = docx2pdf_file

            if conv_save_fg:
                shutil.copyfile(docx2pdf_file , toPdf_file)

            i= i +1
            progress_dialog.setValue(i/file_count *100)
            QCoreApplication.processEvents()

            # 打開 PDF 文件
            pdfFile = open(file, 'rb')
            pdfReader = PyPDF2.PdfReader(pdfFile)

            #記錄每一個檔案的頁數
            mark_page = mark_page+ len(pdfReader.pages)
            BookMark_page.append( mark_page )

            # 循環頁面，將每個頁面添加到新 PDF 文件中
            for pageNum in range(len(pdfReader.pages)):
                pdfWriter_tmp.add_page(pdfReader.pages[pageNum])

            # 關閉 PDF 文件
            pdfFile.close()



        if (bookmake_fg):
            "#加入畫簽 -----------------------------------"
            # 打開舊 PDF 文件
            #pdfFile = open(save_pdf_name + '.pdf', 'rb')
            #pdfReader = PyPDF2.PdfReader(pdfFile)

            # 創建新 PDF 文件
            pdfWriter = PyPDF2.PdfWriter()

            # 添加目錄到新 PDF 文件
            i = 0

            for BookMark_name in bookmaek_list:
                #print(BookMark_name)
                if menu_show_list[i] :
                    pdfWriter.add_outline_item(BookMark_name, BookMark_page[i])

                i = i+1


            # 從舊 PDF 文件循環頁面，將每個頁面添加到新 PDF 文件中
            for pageNum in range(len(pdfWriter_tmp.pages)):
                pdfWriter.add_page(pdfWriter_tmp.pages[pageNum])

            # 保存新 PDF 文件
            pdfOutput = open( save_pdf_name + ".pdf" , 'wb')
            pdfWriter.write(pdfOutput)

            # 關閉所有文件
            pdfOutput.close()

        else:
            # 無書簽檔
            pdfOutput = open(save_pdf_name + ".pdf", 'wb')
            pdfWriter_tmp.write(pdfOutput)
            # 關閉所有文件
            pdfOutput.close()

        wdToPDF.Quit() # 退出Word應用物件

