from PySide6.QtWidgets import QApplication, QMainWindow, QPushButton, QProgressBar
from PySide6.QtCore import QThread, Signal
import time

class WorkerThread(QThread):
    progress_signal = Signal(int)

    def __init__(self):
        super().__init__()

    def run(self):
        for i in range(101):
            time.sleep(0.1)
            self.progress_signal.emit(i)

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.button = QPushButton("Start", self)
        self.button.setGeometry(50, 50, 100, 50)
        self.button.clicked.connect(self.start_worker_thread)

        self.progress_bar = QProgressBar(self)
        self.progress_bar.setGeometry(50, 150, 200, 20)
        self.progress_bar.setValue(0)

        self.setGeometry(100, 100, 300, 250)
        self.show()

    def start_worker_thread(self):
        self.worker_thread = WorkerThread()
        self.worker_thread.progress_signal.connect(self.update_progress_bar)
        self.worker_thread.start()

    def update_progress_bar(self, value):
        self.progress_bar.setValue(value)

if __name__ == "__main__":
    app = QApplication([])
    main_window = MainWindow()
    app.exec()
