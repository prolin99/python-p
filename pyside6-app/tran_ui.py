import argparse

def convert_unicode(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as f:
        content = f.read()
    converted_content = content.encode('utf-8').decode('unicode-escape').encode('utf-8')
    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(converted_content.decode('utf-8'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert unicode in input file to utf-8 in output file')
    parser.add_argument('-i', '--input', type=str, required=True, help='Path to input file')
    parser.add_argument('-o', '--output', type=str, required=True, help='Path to output file')
    args = parser.parse_args()
    convert_unicode(args.input, args.output)
