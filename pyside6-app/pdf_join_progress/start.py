from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import QColor

from main import Ui_MainWindow
from pdf_join_fun import do_join_work
import sys, os ,re

        #指定 self.listWidget.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        #去除 self.listWidget.setMovement(QtWidgets.QListView.Static)
class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.show_color = QColor(0xDF,0xFF, 0xDF)
        self.hide_color = QColor(0x4F,0x4F, 0x4F)

        # 添加应用程序逻辑和事件处理程序
        #選擇檔案
        self.btn_selectFiles.clicked.connect(self.select_files)
        #重新
        self.btn_remove.clicked.connect(self.remove_file)

        #進入編書簽模式
        self.chkEditMode.clicked.connect(self.edit_bookmark)

        #進入 show_hide
        self.chk_Menu_show.clicked.connect(self.ShowHide_menu)

        #listwget 點選模式（show_hide)
        self.listWidget.clicked.connect(self.menu_click)

        #開始合併
        self.Btn_Join.clicked.connect(self.do_join)

        #重新
        self.btn_New.clicked.connect(self.re_new)
        #重新
        self.btn_replace.clicked.connect(self.do_replace)


    def select_files(self):
        #讀取多個 pdf 案
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, '選取 PDF 檔案', '', 'PDF Files (*.pdf)', options=options)

        if files:
            for file in files:
                #呈現過濾過的名稱，完整路徑放在 data(1)
                fn = os.path.basename(file)
                #調整
                beg_st = self.txt_beg.text()
                beg = 0
                if ( beg_st.isdigit() ):
                    beg = int(beg_st)
                rep_s = self.txtRep.text()
                fn = fn[beg:-4]
                #fn= fn.replace(rep_s, "")
                fn = re.sub(rep_s,"", fn, flags=re.IGNORECASE)

                item = QListWidgetItem(fn)  #目錄名
                item.setData(Qt.UserRole, file)       #完整路徑
                item.setData(Qt.UserRole+1, True)       #預設顯示在目錄中


                item.setBackground(self.show_color)
                self.listWidget.addItem(item)

    def remove_file(self):
        selected_items = self.listWidget.selectedItems()  # 获取所选项的列表
        for item in selected_items:
            self.listWidget.takeItem(self.listWidget.row(item))  # 从列表中删除每个所选项



    def edit_bookmark(self):
        #設為可編修或不同編修
        for i in range(self.listWidget.count()):
            item = self.listWidget.item(i)
            if self.chkEditMode.isChecked():
                item.setFlags(item.flags() | Qt.ItemIsEditable)
            else:
                item.setFlags(item.flags() & ~Qt.ItemIsEditable)
        if self.chkEditMode.isChecked() :
            self.chk_Menu_show.setChecked(False)

    def ShowHide_menu(self):
        if self.chk_Menu_show.isChecked() :
            self.chkEditMode.setChecked(False)

    def menu_click(self):
        if self.chk_Menu_show.isChecked() :
            #點選是否出現的目錄上
            selected_items = self.listWidget.selectedItems()  # 获取所选项的列表

            for item in selected_items:
                fg = not item.data(Qt.UserRole+1)
                item.setData(Qt.UserRole+1, fg)
                #print(item.text(), item.data(Qt.UserRole+1) )
                item.setData(Qt.UserRole+1, fg)
                if fg:
                    item.setBackground(self.show_color)
                else:
                    item.setBackground(self.hide_color)

    def do_join(self):
        if (self.listWidget.count() <=0 ):
            QMessageBox.critical(self, '錯誤', "尚未選擇要合併的 pdf 檔案")
            return

        #存檔名稱
        menu_lst = list()
        pdf_lst = list()
        menu_show_list = list()
        items = [self.listWidget.item(i) for i in range(self.listWidget.count())]
        for item in items:
            #取得欄內資料
            #
            menu_lst.append( item.text() )  #名
            pdf_lst.append(item.data(Qt.UserRole))    #完整路徑
            menu_show_list.append(item.data(Qt.UserRole+1))  #是否顯示在目錄中

        #print(menu_lst ,pdf_lst ,menu_show_list )
        #return
        #由第一個檔取得目錄檔名為預設新合併檔名
        last_directory = os.path.dirname(pdf_lst[1])
        save_pdf_name = last_directory
        save_file_path, _ = QFileDialog.getSaveFileName(self, "新合併 PDF", "./" +save_pdf_name + '.pdf', "PDF檔(*.pdf)")

        if (save_file_path):
            # 進度顯示
            progress_dialog = QProgressDialog(self)
            progress_dialog.setWindowTitle("檔案轉換中")
            progress_dialog.setLabelText("合併成 PDF ...")
            progress_dialog.setRange(0, 100)
            progress_dialog.setCancelButton(None)
            progress_dialog.show()
            #print(save_file_path)
            #把進度畫面傳入
            do_join_work(pdf_lst , menu_lst , menu_show_list , self.Chk_bookMark.isChecked()   ,  save_file_path  , progress_dialog )
            #進度隱藏
            progress_dialog.accept()
            QMessageBox.information(self, '合併完成', "新檔名：" + save_file_path )
        else:
            print('放棄')

    def re_new(self):
        # 显示一个消息框，询问用户是否要保存文件
        reply = QMessageBox.question(self, '確認', '是否要清除所有設定？', QMessageBox.Yes | QMessageBox.No )

        if reply == QMessageBox.Yes:
            self.listWidget.clear()

    def do_replace(self):
        items = [self.listWidget.item(i) for i in range(self.listWidget.count())]
        for item in items:
            rep_s = self.txtRep.text()
            rep_new = self.txt_rep_new.text()
            item_text = item.text()
            #item_text = item_text.replace(rep_s, rep_new)
            #不區分大小寫
            item_text = re.sub(rep_s,rep_new, item_text, flags=re.IGNORECASE)
            item.setText(item_text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())