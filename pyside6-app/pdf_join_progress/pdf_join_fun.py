import os ,time
import PyPDF2
from PySide6.QtCore import *
from PySide6.QtWidgets import *


def do_join_work(pdfFiles_list , bookmaek_list , menu_show_list , bookmake_fg   ,  save_pdf_name , progress_dialog):
    # 獲取當前工作目錄

    BookMark_page=[0]
    if save_pdf_name.lower().endswith(".pdf"):
        save_pdf_name = save_pdf_name[:-4]
        print(save_pdf_name)

    # 合併成一個 PDF --------------------------------
    print("合併成一個 PDF --------------------------------")
    # 創建新 PDF 文件
    pdfWriter_tmp = PyPDF2.PdfWriter()

    mark_page = 0
    i = 0
    for file in pdfFiles_list:
        # 打開 PDF 文件
        pdfFile = open(file, 'rb')
        pdfReader = PyPDF2.PdfReader(pdfFile)

        #記錄每一個檔案的頁數
        mark_page = mark_page+ len(pdfReader.pages)
        BookMark_page.append( mark_page )

        # 循環頁面，將每個頁面添加到新 PDF 文件中
        for pageNum in range(len(pdfReader.pages)):
            pdfWriter_tmp.add_page(pdfReader.pages[pageNum])

        # 關閉 PDF 文件
        pdfFile.close()
        i = i + 10
        progress_dialog.setValue(i)
        QCoreApplication.processEvents()
        #time.sleep(5)

    # 保存新 PDF 文件
    #pdfOutput = open(save_pdf_name + ".pdf", 'wb')
    #pdfWriter.write(pdfOutput)


    # 關閉所有文件
    #pdfOutput.close()

    if (bookmake_fg):
        "#加入畫簽 -----------------------------------"
        # 打開舊 PDF 文件
        #pdfFile = open(save_pdf_name + '.pdf', 'rb')
        #pdfReader = PyPDF2.PdfReader(pdfFile)

        # 創建新 PDF 文件
        pdfWriter = PyPDF2.PdfWriter()

        # 添加目錄到新 PDF 文件
        i = 0

        for BookMark_name in bookmaek_list:
            #print(BookMark_name)
            if menu_show_list[i] :
                pdfWriter.add_outline_item(BookMark_name, BookMark_page[i])

            i = i+1


        # 從舊 PDF 文件循環頁面，將每個頁面添加到新 PDF 文件中
        for pageNum in range(len(pdfWriter_tmp.pages)):
            pdfWriter.add_page(pdfWriter_tmp.pages[pageNum])

        # 保存新 PDF 文件
        pdfOutput = open( save_pdf_name + ".pdf" , 'wb')
        pdfWriter.write(pdfOutput)

        # 關閉所有文件
        pdfOutput.close()

    else:
        # 無書簽檔
        pdfOutput = open(save_pdf_name + ".pdf", 'wb')
        pdfWriter_tmp.write(pdfOutput)


        # 關閉所有文件
        pdfOutput.close()

