import tkinter as tk

#grid (格狀分列)

class Window(tk.Tk):
    def __init__(self):
        super().__init__()
        #視窗標題
        self.title("Hello Tkinter")
        self.geometry("400x220")

        #上下間隔 20
        self.rowconfigure(0, pad=20)

        self.label1 = tk.Label(self)
        self.label1["text"] = 'Name'
        self.label1.grid(row=0 , column= 0 )

        self.entry_name = tk.Entry()
        self.entry_name.grid(row=0 , column= 1)

        # weight =1 可依畫面大小移動調整
        self.rowconfigure(1, weight=1)

        self.label2 = tk.Label(self)
        self.label2["text"] = 'Password'
        self.label2.grid(row=1 , column= 0 )

        self.entry_pwd = tk.Entry()
        self.entry_pwd.grid(row=1 , column= 1)

        #sticky NWSE 對齊方向
        self.button = tk.Button(self)
        self.button["text"] = "real name"
        self.button["command"] = self.say_hello
        self.button.grid(row=2, column=1 ,sticky=tk.W + tk.N)

        #預設置中
        self.button2 = tk.Button(self)
        self.button2["text"] = "close"
        self.button2["command"] = self.close
        self.button2.grid(row=3, column=1   )


    def say_hello(self):
        # label 可被修改
        self.label1.configure(text= "real name")

    def close(self):
        #視窗結束
        self.destroy()


if __name__ == "__main__":
    window = Window()
    window.mainloop()