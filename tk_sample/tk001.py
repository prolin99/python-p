import tkinter as tk

#pack 依物件排列 side 分上下左右位置擺放

class Window(tk.Tk):
    def __init__(self):
        super().__init__()
        #視窗標題
        self.title("Hello Tkinter")
        
        #字串標題列 text 文字 bg 背景色
        self.label = tk.Label(self, text="Hello World!" , bg="red")
        # fill  expand  padx 水平軸間隔（左,右）
        self.label.pack(fill=tk.BOTH, expand=1, padx=100, pady=50)

        #按鍵 text 文字 height, width 高寬字數 command 點擊後執行函數
        self.btn = tk.Button(self, text='say hello' , height=2 , width=10 , command= self.say_hello)
        self.btn.pack(side=tk.LEFT,padx=(20,0), pady=(0,20))

        self.btn_exit = tk.Button(self, text='close' ,height=2 , width=10 , command= self.close )
        self.btn_exit.pack(side=tk.RIGHT,padx=(0,20), pady=(0,20))

    def say_hello(self):
        # label 可被修改
        self.label.configure(text= "button Clicked")

    def close(self):
        #視窗結束
        self.destroy()


if __name__ == "__main__":
    window = Window()
    window.mainloop()