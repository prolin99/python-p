import tkinter as tk

#place (xy)

class Window(tk.Tk):
    def __init__(self):
        super().__init__()
        #視窗標題
        self.title("Hello Tkinter")
        self.geometry("400x220")

        self.label1 = tk.Label(self)
        self.label1["text"] = 'Name'
        self.label1.place(x = 20, y = 30  , width=120 )


        self.entry_name = tk.Entry()
        self.entry_name.place(x = 150, y = 30  , width=120 )

        self.label2 = tk.Label(self)
        self.label2["text"] = 'Password'
        self.label2.place(x = 20, y = 70  , width=120, height=25)

        self.entry_pwd = tk.Entry()
        self.entry_pwd.place(x = 150, y = 70  , width=120, height=25)


        #sticky NWSE 對齊方向
        self.button = tk.Button(self)
        self.button["text"] = "real name"
        self.button["command"] = self.say_hello
        self.button.place(x = 40, y = 120  )

        #預設置中
        self.button2 = tk.Button(self)
        self.button2["text"] = "close"
        self.button2["command"] = self.close
        self.button2.place(x = 160, y = 120  )



    def say_hello(self):
        # label 可被修改
        self.label1.configure(text= "real name")

    def close(self):
        #視窗結束
        self.destroy()


if __name__ == "__main__":
    window = Window()
    window.mainloop()