import pandas as pd 
df = pd.read_excel("stud_test1.xlsx" , engine='openpyxl')

#列出 count筆數、比均、最大最小等
print(df.describe())

#單科總和 sum() 平均 mean  最小min() 最大 max() 筆數 count()
print(df['國語'].mean())


#第一種方法
#使用 水平軸算平均(但要注意有些數字欄位是不是成績，例如座號等數字)
df['平均 mean']= df.mean(axis=1 )

#第二種方法
#加入個人總分、平均
#df['總分'] = df['國語']+df['數學']+df['社會']
#df['平均'] = df['總分']/3

print(df)

#取得部份資料 女性
df_female= df[df['性別']=='female']
print(df_female)

#女生 並為 group B
df_female_GroupB = df[(df['性別']=='female') & (df['組別'] == 'group B')]
print(df_female_GroupB)


#組別欄位中有 B 字元
df_GroupB = df[df['組別'].str.contains('B')]
print(df_GroupB)