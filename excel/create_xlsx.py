#匯出 excel files
#pip3 install xlsxwriter
import xlsxwriter as xls

workbook = xls.Workbook('t.xlsx')
worksheet = workbook.add_worksheet()

data = (
['aa',100],
['ab',1000],
['ddd',2000]
)

row=0
col=0
for item,cost in (data):
    worksheet.write(row,col , item)
    worksheet.write(row,col+1 , cost )
    row +=1
worksheet.write(row,col , 'total')
worksheet.write(row, col + 1 , '=SUM( B1:B3 ) ')
#如果無法自動計算，要修改試算表軟體，開啟時自動重新計算
workbook.close()
