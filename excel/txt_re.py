# 安裝 PYTHON 3
# 需要再安裝模組
# pip install openpyxl
# pip install pandas
# 圖書系統匯出檔  0000.dat ，執行後會轉出 0000.xlsx 檔
# 可能需互分次轉，避免檔案過大無法處理

import re

import pandas as pd

print('開始轉換.....')

with open('003', encoding='BIG5'  ) as file_object:
    contents = file_object.read()
    #每筆資料以 nam 開頭
    rows = contents.split('nam')


d_rows=[]
for row in rows:
    print('.',end='')
    
    #多項以 \x1e 分列
    cols = re.split('\x1e', row)
    d_row=[]
    
    for col in cols:
        #分格
        cols_in  = re.split('\x1f|\x1d', col)
        
        col_i = 0
        for col_in in cols_in :
            
            col_value= re.sub('^[a-z]','',col_in)
            d_row.append(col_value)

            col_i += 1
        for col_add in range(col_i ,10):
            d_row.append('')





    d_rows.append(d_row)



print('存檔中...')

df = pd.DataFrame(d_rows)

df.to_excel('0000.xlsx' )
print('ok')
