#!/usr/bin/python3
#http://monkeycoding.com/?p=373
#說明：在一般耗時工作，會造成 UI 界面卡住的情形，使用 QThread ，解決這個問題
import sys ,os

from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *
from PySide2.QtCore import QFile, QObject , Qt , QTimer ,QTime ,QThread ,Signal ,Slot

class Form(QObject):
    mode_v=0
    def __init__(self,ui_file, parent=None):
        super(Form,self).__init__(parent)
        #載入 UI 介面
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        #按鍵關連的動作
        self.window.btn_close.clicked.connect(app.exit)
        self.window.btn_start.clicked.connect(self.doit)
        self.window.btn_start2.clicked.connect(self.doit2)
        self.window.btn_old.clicked.connect(self.old_doit)

        self.window.show()

        #計時部份，顯示時間
        myTimer = QTimer(self)
        myTimer.timeout.connect(self.showTime)
        myTimer.start(1000)

        #多工執行緒 OtherThread class
        self.thread = OtherThread(self,1)
        self.thread.over.connect(self.finishCalc)

        self.thread2 = OtherThread(self,2)
        self.thread2.over.connect(self.finishCalc)

        self.window.lab_v.setText('v:')
    def showTime(self):
        time = QTime.currentTime()
        t= time.toString("hh:mm:ss")
        self.window.lcdNumber.display(t)


    #執行緒結束時
    #@QtCore.Slot(object)
    @Slot(object)
    def finishCalc(self, value):
        print("end: %s" %value)
        QMessageBox.information(self.window,'info','計算結束')

    #耗時工作開始
    def doit(self):
        mode_v=1
        self.thread.start()

    def doit2(self):
        self.thread2.start()

    #非多緒執行界面會卡住
    def old_doit(self):
        for i in range(300000):
            print(i)
            a =0
        print('end')
        return('finish')

#多工執行緒
class OtherThread(QThread):
    #QtCore.Signal(object)
    over =  Signal(object)
    mv=0
    def __init__(self,p,v):
        super().__init__()

        self.mv=v
        print(self.mv)
    def run(self):
        for i in range(100000):
            #print(i)
            #在界面上顯示狀態
            form.window.lab_v.setText(str(self.mv) +'--'+ str(i))
            for j in range(30000):
                a =0
        #傳回值
        self.over.emit('finish')



if __name__ =='__main__':
    app = QApplication(sys.argv)
    form = Form('main.ui')
    sys.exit(app.exec_())
