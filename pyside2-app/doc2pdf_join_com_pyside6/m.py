from PySide6.QtCore import *
from PySide6.QtWidgets import *

class MyWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        layout = QVBoxLayout()
        button = QPushButton('Select files')
        button.clicked.connect(self.select_files)
        layout.addWidget(button)
        self.setLayout(layout)

    def select_files(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, 'Select files', '', 'All Files (*);;Text Files (*.txt)', options=options)
        if files:
            print('Selected files:', files)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    widget = MyWidget()
    widget.show()
    sys.exit(app.exec())
