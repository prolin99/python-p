#! /usr/bin/python3
# 文字檔 轉換成 epub
# 
import sys ,os , zipfile ,shutil ,tempfile ,time
import re ,random

#簡繁轉換模組
from opencc import OpenCC

#所在程式目錄
ScriptPath = os.path.dirname(sys.argv[0])
#print(ScriptPath)
#暫存區的
SavePath = tempfile.gettempdir()
tmpEpubPath = '/epub%s' % random.randint(1000,9999)
#print(SavePath)

openCC = OpenCC()
openCC.set_conversion('s2twp')

def unzip_epub( epubfn ):
    #解壓縮 epub
    with zipfile.ZipFile(epubfn, 'r') as myzip:
        for file in myzip.namelist():
            myzip.extract(file,SavePath + tmpEpubPath )


def file_Ts(fn):
    os.path.isfile
    file= open(fn,"r",encoding='utf8')
    data = file.read()
    file.close()
    data=openCC.convert(data)

    data= re.sub('xml:lang="zh-CN"','xml:lang="zh-TW"',data)
    data= re.sub('<dc:language>zh-cn</dc:language>','<dc:language>zh</dc:language>',data)

    file= open(fn,"w",encoding='utf8')
    file.write(data)
    file.close()
    #file

def path_ts_do(path):
    #xhtml  htm html opf ncx
    included_extenstions = ['xhtml', 'html', 'htm', 'opf' , 'ncx' ,'xml']
    # walk 遞迴取檔
    for root,d,f in os.walk(path):
        for file in f:
             if any(file.endswith(ext) for ext in included_extenstions):
                 #print( os.path.join(root,file) )
                 file_Ts(os.path.join(root,file))

    return True


def do_trans(txtfn , book , auth):

    #解開 epub
    unzip_epub(txtfn)
    #開始轉換 簡轉繁
    success = path_ts_do(SavePath + tmpEpubPath )

    if success :

        #壓縮 成 epub，存在同目錄
        newpubfn=book +'_'+ auth +'.epub'
        zf = zipfile.ZipFile(os.path.dirname(txtfn) + '/'+newpubfn , mode='w')#只儲存不壓縮
        os.chdir(SavePath + tmpEpubPath)
        for root, folders, files in os.walk("./"):
            for sfile in files:
                aFile = os.path.join(root, sfile)
                zf.write(aFile)
        zf.close()


        #message
        return True
    else:
        return False
    #移除工作檔
    shutil.rmtree(SavePath + tmpEpubPath , ignore_errors=True)
