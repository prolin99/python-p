#!/usr/bin/python3
import sys ,os

from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *
from PySide2.QtCore import QFile, QObject

import trans_ts
class Form(QObject):
    txtfn =''
    txtpath=''
    tmpepub_fn=''
    def __init__(self,ui_file, parent=None):
        super(Form,self).__init__(parent)
        #載入 UI 介面
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        #關連的動作
        self.window.btn_close.clicked.connect(app.exit)
        self.window.btn_epub.clicked.connect(self.toepub)
        self.window.btn_selete_Fn.clicked.connect(self.select_txt)

        self.window.show()

    def toepub(self):
        bookname = self.window.edit_book.text()
        author= self.window.edit_author.text()
        success = trans_ts.do_trans(self.txtfn , bookname , author)
        msgBox = QMessageBox()
        if success:
            msgBox.setText(" Epub 簡轉繁成功！")
        else :
            msgBox.setText(" Epub 簡轉繁 失敗！")
        msgBox.exec_()

    def select_txt(self):
        fileName = QFileDialog.getOpenFileNames(caption='選擇EPUB檔' , filter='epub files(*.epub)')
        if fileName:
            self.txtfn = fileName[0][0]
            self.window.edit_txt.setText(self.txtfn)
            self.txtpath = os.path.split(self.txtfn)[0]
            self.tmpepub_fn = os.path.split(self.txtfn)[1]
            self.tmpepub_fn = tmpepub_fn =self.tmpepub_fn[:-5]
            self.window.edit_book.setText(self.tmpepub_fn)

if __name__ =='__main__':
    app = QApplication(sys.argv)
    form = Form('tc.ui')
    sys.exit(app.exec_())
