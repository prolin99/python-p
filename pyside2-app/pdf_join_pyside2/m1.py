# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PySide6 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1003, 708)
        font = QtGui.QFont()
        font.setPointSize(14)
        MainWindow.setFont(font)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 50, 571, 621))
        self.listWidget.setDragEnabled(False)
        self.listWidget.setDragDropOverwriteMode(False)

        self.listWidget.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        self.listWidget.setEditTriggers(QtWidgets.QAbstractItemView.AllEditTriggers)

        self.listWidget.setObjectName("listWidget")
        self.btn_selectFiles = QtWidgets.QPushButton(self.centralwidget)
        self.btn_selectFiles.setGeometry(QtCore.QRect(10, 20, 221, 30))
        self.btn_selectFiles.setObjectName("btn_selectFiles")
        self.Chk_bookMark = QtWidgets.QCheckBox(self.centralwidget)
        self.Chk_bookMark.setGeometry(QtCore.QRect(590, 60, 171, 41))
        self.Chk_bookMark.setObjectName("Chk_bookMark")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(590, 110, 141, 21))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(590, 170, 131, 21))
        self.label_2.setObjectName("label_2")
        self.txt_beg = QtWidgets.QLineEdit(self.centralwidget)
        self.txt_beg.setGeometry(QtCore.QRect(790, 110, 113, 29))
        self.txt_beg.setObjectName("txt_beg")
        self.txtRep = QtWidgets.QLineEdit(self.centralwidget)
        self.txtRep.setGeometry(QtCore.QRect(790, 170, 113, 29))
        self.txtRep.setObjectName("txtRep")
        self.Btn_Join = QtWidgets.QPushButton(self.centralwidget)
        self.Btn_Join.setGeometry(QtCore.QRect(600, 590, 131, 30))
        self.Btn_Join.setObjectName("Btn_Join")
        self.btn_SavePath = QtWidgets.QPushButton(self.centralwidget)
        self.btn_SavePath.setGeometry(QtCore.QRect(590, 230, 161, 30))
        self.btn_SavePath.setObjectName("btn_SavePath")
        self.txt_newPdf = QtWidgets.QLineEdit(self.centralwidget)
        self.txt_newPdf.setGeometry(QtCore.QRect(590, 300, 371, 29))
        self.txt_newPdf.setObjectName("txt_newPdf")
        self.lab_savePath = QtWidgets.QLabel(self.centralwidget)
        self.lab_savePath.setGeometry(QtCore.QRect(590, 270, 80, 21))
        self.lab_savePath.setObjectName("lab_savePath")
        self.chkEditMode = QtWidgets.QCheckBox(self.centralwidget)
        self.chkEditMode.setGeometry(QtCore.QRect(300, 20, 241, 27))
        self.chkEditMode.setObjectName("chkEditMode")        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1003, 39))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btn_selectFiles.setText(_translate("MainWindow", "要合併的 PDF 檔案"))
        self.Chk_bookMark.setText(_translate("MainWindow", "是否加書簽"))
        self.label.setText(_translate("MainWindow", "略去開頭字數："))
        self.label_2.setText(_translate("MainWindow", "清除多餘文字："))
        self.Btn_Join.setText(_translate("MainWindow", "執行合併"))
        self.btn_SavePath.setText(_translate("MainWindow", "存檔目錄："))
        self.lab_savePath.setText(_translate("MainWindow", "TextLabel"))
        self.chkEditMode.setText(_translate("MainWindow", "編修書簽模式"))