#!/usr/bin/python3
import sys ,os

from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *
from PySide2.QtCore import QFile, QObject , Qt , QThread ,Signal ,Slot

#多核


import t2epub

class Form(QObject):
    txtfn =''
    txtpath=''
    tmpepub_fn=''

    def __init__(self,ui_file, parent=None):
        super(Form,self).__init__(parent)
        #載入 UI 介面
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        #關連的動作
        self.window.btn_close.clicked.connect(app.exit)
        self.window.btn_epub.clicked.connect(self.toepub)
        self.window.btn_selete_Fn.clicked.connect(self.select_txt)
        #self.window.rb_code1.clicked.connect(lambda: self.txt_code_set(0))

        self.window.show()

        #由外部參數指定文字檔 （要使用目錄位置）
        if len(sys.argv)>=2 :
            self.txt_file_selected( sys.argv[1])



    #執行緒結束時
    #@QtCore.Slot(object)
    @Slot(object)
    def finishCalc(self, value):
        print("end: %s" %value)
        #QMessageBox.information(self.window,'info','計算結束')
        msgBox = QMessageBox(self.window)
        if value:
            QMessageBox.information(self.window,'info','文字檔轉換 Epub 成功！')
        else :
            QMessageBox.information(self.window,'info','文字檔轉換 Epub 失敗！')

        self.window.btn_epub.setText('轉換 Epub')
        form.window.btn_epub.setEnabled(True)

    def toepub(self):
        if self.txtfn=='' :
            return()



        bookname = self.window.edit_book.text()
        author= self.window.edit_author.text()
        vh = self.window.chk_vh.isChecked()
        if self.window.rb_code1.isChecked(): txtcode=0
        if self.window.rb_code2.isChecked(): txtcode=1
        if self.window.rb_code3.isChecked(): txtcode=2
        if self.window.rb_code4.isChecked(): txtcode=3
        if self.window.rb_code5.isChecked(): txtcode=4

        if self.window.rb_trans1.isChecked(): trans=0
        if self.window.rb_trans2.isChecked(): trans=1
        if self.window.rb_trans3.isChecked(): trans=2

        zh_tw_fg = self.window.chk_zh_tw.isChecked()


        #print("{}{}{}{}{}" . format(bookname , author ,vh ,txtcode ,trans ))
        #epub 架構檔
        t2epub.unzip_epub()
        #開始轉換

        #多工執行緒 OtherThread class
        self.thread = doThread(self ,self.txtfn ,bookname , author  ,trans ,vh , txtcode ,zh_tw_fg)
        self.thread.over.connect(self.finishCalc)
        self.thread.start()

        #success = t2epub.txt2html(self.txtfn ,bookname , author  ,trans ,vh , txtcode ,zh_tw_fg )


    def select_txt(self):
        fileName = QFileDialog.getOpenFileNames(parent=self.window , caption='選擇文字檔' , filter='Text files(*.txt)')

        if fileName[0] !=[]:
            self.txt_file_selected(fileName[0][0])
            #self.txtfn = fileName[0][0]
            #self.window.edit_txt.setText(self.txtfn)
            #self.txtpath = os.path.split(self.txtfn)[0]
            #self.tmpepub_fn = os.path.split(self.txtfn)[1]
            #self.tmpepub_fn = tmpepub_fn =self.tmpepub_fn[:-4]
            #self.window.edit_book.setText(self.tmpepub_fn)

    def txt_file_selected(self ,txtfn):
        self.txtfn = txtfn
        self.window.edit_txt.setText(self.txtfn)
        self.txtpath = os.path.split(self.txtfn)[0]
        self.tmpepub_fn = os.path.split(self.txtfn)[1]
        self.tmpepub_fn = tmpepub_fn =self.tmpepub_fn[:-4]
        self.window.edit_book.setText(self.tmpepub_fn)


#多工執行緒
class doThread(QThread):
    #QtCore.Signal(object)
    over =  Signal(object)
    mv=0
    def __init__(self,p, txtfn ,bookname , author  ,trans ,vh , txtcode ,zh_tw_fg ):
        super().__init__()
        self.txtfn = txtfn
        self.bookname = bookname
        self.author = author
        self.trans = trans
        self.vh = vh
        self.txtcode = txtcode
        self.zh_tw_fg = zh_tw_fg


    def run(self):
        form.window.btn_epub.setText('轉換中.....')
        form.window.btn_epub.setEnabled(False)

        self.success = t2epub.txt2html(self.txtfn ,self.bookname , self.author  ,self.trans ,self.vh , self.txtcode ,self.zh_tw_fg )
        #傳回值
        self.over.emit(self.success)


if __name__ =='__main__':
    ScriptPath = os.path.dirname(sys.argv[0])
    app = QApplication(sys.argv)
    form = Form(ScriptPath + '/main.ui')


    sys.exit(app.exec_())
