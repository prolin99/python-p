import sys ,os , zipfile ,shutil ,tempfile ,time
import re
#簡繁轉換模組
from opencc import OpenCC

#找文字檔編碼
import chardet

import html

#所在程式目錄
ScriptPath = os.path.dirname(sys.argv[0])


#暫存區的
SavePath = tempfile.gettempdir()


def unzip_epub():
    #解壓縮 epub
    print("SavePath {}".format(SavePath))
    with zipfile.ZipFile(ScriptPath + '/epub-src2.zip', 'r') as myzip:
        for file in myzip.namelist():
            myzip.extract(file,SavePath + '/epub')


html_beg='''<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-TW" xmlns:xml="http://www.w3.org/XML/1998/namespace">
<head>
  <!-- InstanceBeginEditable name="doctitle" -->

  <title>%s</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- InstanceEndEditable -->  <link href="../Styles/%s.css" rel="stylesheet" type="text/css"/>
<!-- InstanceBeginEditable name="head" -->  <!-- InstanceEndEditable -->
</head>

<body %s >
  <div>
    <div>
      <!-- InstanceBeginEditable name="Content" -->
'''


html_end='''
    </div>
<!--End content-->  </div>
<!--End ADE-->  <!-- InstanceEnd -->
</body>
</html>
'''

navpoint="""
              <navPoint id="navPoint-%d" playOrder="%d">
                <navLabel>
                  <text>%s</text>
                </navLabel>
                <content src="Text/ch%d.html"/>
              </navPoint>
"""

tocdoc="""<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN"
 "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd"><ncx version="2005-1" xmlns="http://www.daisy.org/z3986/2005/ncx/">
  <head>
    <meta content="urn:uuid:eba0e149-7241-43f3-a563-7b132597c3a5" name="dtb:uid"/>
    <meta content="1" name="dtb:depth"/>
    <meta content="0" name="dtb:totalPageCount"/>
    <meta content="0" name="dtb:maxPageNumber"/>
  </head>
  <docTitle>
    <text>%s</text>
  </docTitle>
  <navMap>
    %s
  </navMap>
</ncx>
"""

content_beg="""<?xml version="1.0" encoding="utf-8"?>
<package version="2.0" unique-identifier="BookID" xmlns="http://www.idpf.org/2007/opf">
  <metadata xmlns:opf="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/">
    <dc:title>%s</dc:title>
    <dc:creator opf:role="aut">%s</dc:creator>
    <dc:language>zh</dc:language>
    <dc:identifier opf:scheme="UUID" id="BookID">urn:uuid:8c7b74e5-d8c5-40e1-bf6f-55cbc5db9c1f</dc:identifier>
    <meta content="0.9.9" name="Sigil version" />
    <dc:date opf:event="modification">%s</dc:date>
  </metadata>
  <manifest>
    <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml"/>
    <item id="Style.css" href="Styles/Style.css" media-type="text/css"/>
    <item id="VStyle.css" href="Styles/VStyle.css" media-type="text/css"/>
    %s
  </manifest>
  <spine toc="ncx">
    %s
  </spine>
  <guide>
  </guide>
</package>
"""



#文字編碼方式
'''
def get_txt_encode(fn):
    #encodings 順序會造成正確性
    encodings = [  'utf-8',  'big5', 'GBK' ,'utf-16'  ]
    txt_code='big5'
    for e in encodings:
        try:
            fh = open(fn, 'r', encoding=e)
            fh.readlines()
            fh.seek(0)
        except UnicodeDecodeError:
            print('got unicode error with %s , trying different encoding' % e)
        else:
            txt_code = e
            print('opening the file with encoding:  %s ' % e)
            fh.close()
            break

    return txt_code
'''
def get_txt_encode(fn):
    rawdata = open(fn, "rb").read()
    result = chardet.detect(rawdata)
    charenc = result['encoding']
    #如無法偵測 預設 big5
    if charenc == None:
        charenc='Big5'
    #print(charenc)
    return charenc

#寫入分吤章節 html
def savepage(page,doc):
    fn = SavePath + ('/epub/OEBPS/Text/ch%d.html' % page)
    file= open(fn,'w' ,encoding='utf8')
    file.write( doc + html_end)
    file.close()

def saveToc(navpoint):
    fn = SavePath + '/epub/OEBPS/toc.ncx'
    #print(fn)
    file= open(fn,'w', encoding='utf8')
    file.write(tocdoc % ('book',navpoint) )
    file.close()

def savecontent(book_name, auth_name, page):
    fn = SavePath + '/epub/OEBPS/content.opf'
    #print(fn)
    item=''
    itemref=''

    file= open(fn,'w', encoding='utf8')
    today= time.strftime("%Y-%m-%d", time.localtime())

    for i in range(101,page+1):
        item= item+ '   <item id="ch%d.html" href="Text/ch%d.html" media-type="application/xhtml+xml"/>\n' %(i,i)
        itemref=itemref + ' <itemref idref="ch%s.html"/>\n' % i

    file.write(content_beg % (book_name ,auth_name,today , item , itemref)  )
    file.close()



#讀取文字檔
def txt2html( fn ,book_name , auth_name  ,ts ,VH , decode ,zh_tw_fg):

    page=101
    line_num=1
    if VH:

        style='VStyle'
        body_class=' class="vertical"'

    else:
        style='Style'
        body_class=''


    if zh_tw_fg:
        #讀取 zh-tw 詞句修改檔
        chk_file = open(ScriptPath + '/zh-tw_chk.txt',"r")
        chk_lines = chk_file.readlines()
        z2t=[]
        for zhtwline in  chk_lines:
            zhtwline= zhtwline.strip()
            z2tword = zhtwline.split(',')
            z2t.append(z2tword)


    #print(body_class)
    html_doc= html_beg % ("首頁" ,style, body_class)

    nav = navpoint %(101,101,'首頁',101)



    try:

        #編碼

        if decode==3:
            dc='GBK'
            #file = open(fn,'r').read().decode("GBK").encode("utf8")
            file= open(fn,"rb")
        elif decode==2:
            dc='BIG5'
            #file = open(fn,'r').read().decode("big5").encode("utf8")
            file= open(fn,"rb")
        elif decode==4:

            dc=''
            file= open(fn,"r",encoding='utf-16')
        elif decode==0:
            #自動判別
            #dc=''
            dc = get_txt_encode(fn)
            print('auto--{}'.format(dc))

            #windows-1252 改為預設 big5
            if dc=='Windows-1252':
                dc='BIG5'

            # UTF 開啟方式不同
            if dc[:3]=="UTF" :
                file= open(fn,"r",encoding=dc)
                dc=''
            else:
                file= open(fn,"rb")
        else:
            dc=''
            file= open(fn,"r",encoding='utf8')


        #簡繁轉換
        openCC = OpenCC()
        if ts == 1:
            #簡轉繁
            openCC.set_conversion('s2twp')
        if ts == 2:
            #繁轉簡
            openCC.set_conversion('tw2s')


        for line in file.readlines():
            if len(line.strip()) > 0 :
                if dc != '' :
                    try:
                        #轉碼錯誤忽略
                        line=str(line.decode(dc ,'ignore') )
                    except:
                        result=False
                        print( 'err-%s --%s' % (line_num , line) )

                #清除 \x00  不正常的 utf8 碼
                line = line.replace('\x00', '')
                line = line.replace('\x01', '')
                #\xE2 80 83、空白 改回半型空白
                line = line.replace(' ', ' ')
                #\xE2 80 82、空白 改回半型空白
                line = line.replace(' ', ' ')

                if ts>0:
                    try:
                        nline=''
                        nline = openCC.convert(line)
                        line=nline
                    except:
                        print('簡繁- %s' % line )

                line = line.strip()  #移除頭尾特定字符如空白



                #簡繁詞句再置換
                if zh_tw_fg:
                    for zhtw in  z2t:
                        line = line.replace(zhtw[0], zhtw[1])
                        #print(zhtw)

            #print( 'ok-%s --%s  %s' % (line_num , line ,type(line)) )
            #強迫檢查、轉換
            if str(type(line)) == "<class 'bytes'>":
                line= line.decode()
            #print( 'ok-%s --%s  %s' % (line_num , line ,type(line)) )
            line_num = line_num +1

            if line:

                match = False
                if len(line)<30:

                    #章節
                    pattern = re.compile(r'第.+(篇|卷|章|節)')
                    # 取得匹配結果，無法匹配返回 None
                    match = pattern.search(line)
                    if not match :
                        pattern = re.compile(r'(前言|後記)')
                        match = pattern.search(line)




                if (line_num > 500) :
                    savepage(page , html_doc)
                    page=page+1
                    html_doc= html_beg % ("續頁" ,style, body_class)
                    line_num =1


                if match :
                    if line_num >3:   #避免內連續分頁
                        savepage(page , html_doc)
                        page=page+1
                        html_doc= html_beg % ("續頁" ,style, body_class)
                        line_num =1

                        html_doc= html_beg %  (html.escape(line) , style,  body_class)
                        html_doc = html_doc + '<h1 id="heading_id_2">%s</h1>\n' % html.escape(line)
                        nav = nav + navpoint % (page, page, html.escape(line), page)
                    else:
                        html_doc= html_doc + '<p>' + html.escape(line)  +'</p>\n'
                        line_num = 1

                else:

                    html_doc= html_doc + '<p>' + html.escape(line)  +'</p>\n'
                result = True
    except Exception as e :
        print('err: %s' % str(e) )
        result=False

    finally:
        print('end' )
        file.close()

    if result:
        savepage(page , html_doc)
        #章節選單
        if nav:
            saveToc(nav)
        #content
        savecontent(book_name, auth_name, page)

        #把 HTML 檔案再壓縮 成 epub，存在同目錄
        newpubfn=book_name +'_'+ auth_name +'.epub'
        zf = zipfile.ZipFile(os.path.dirname(fn) + '/'+newpubfn , mode='w')#只儲存不壓縮
        os.chdir(SavePath +'/epub')
        for root, folders, files in os.walk("./"):
            for sfile in files:
                aFile = os.path.join(root, sfile)
                zf.write(aFile)
        zf.close()
        # chdir 會影響後續工作，所以再引導回文字目錄
        os.chdir(os.path.dirname(fn))
        #移除工作檔
        shutil.rmtree(SavePath +'/epub' , ignore_errors=True)

    return result
