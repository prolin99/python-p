#! /usr/bin/python3
#把 PDF 檔轉成多張 jpd
#需求 pip3 install pdf2image

from pdf2image import convert_from_path


inputpath = "/home/user/Download/201809s.pdf"
outputpath = "/home/user/Download/images/"

pages = convert_from_path(inputpath , dpi=200, output_folder=None, first_page=None, last_page=None, fmt='ppm' )
i=1
for page in pages:
    page.save( outputpath +'pic%03d.jpg' % (i), 'JPEG')
    i = i +1
