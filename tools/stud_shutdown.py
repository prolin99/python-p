#! /usr/bin/python3
#登出指定的學生機
import os
remote_ip='120.116.25.'
num = int( input('要關機的學生主機代號(0代表結束)：') )

while num>0:
    cmd= "rsh %s%s sudo shutdown -h now &" %(remote_ip , num)
    print('關機... %s' %cmd)
    os.system(cmd)
    num = int( input('要關機的學生主機代號(0代表結束)：') )
