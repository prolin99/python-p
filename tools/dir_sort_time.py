#! /usr/bin/python3
#以建製時間列出檔案

import os
path = "/home/user/Download"

#名稱做排序
#依大小寫
fileslist =os.listdir(path)
fileslist.sort()
print(fileslist)

#反向
fileslist.sort(reverse=True)
print(fileslist)

#忽略大小寫做排序
fileslist = sorted(os.listdir(path) , key=lambda s: s.lower() )
for fn in fileslist:
    if os.path.isfile(os.path.join(path,fn)):
        print(fn)


#依檔案時間排序
fileslist.sort(key=lambda fn: os.path.getmtime(os.path.join(path, fn)))
print(fileslist)
