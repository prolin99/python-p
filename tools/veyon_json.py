#! /usr/bin/python3
# 讀取 mac ，產生 veyon 的部份的 json
# 先把 Veyon 匯出一份再把這些部份取代 LocalData 節處

import uuid

defclassname='CLASS1'
defclass_uuid = uuid.uuid1()

maclist=[]
file= open("maclist.txt","r")
for line in file.readlines():
    line =  line.strip()    #移除頭尾特定字符如空白
    #- 改為：
    line = line.replace('-',':')
    maclist.append(line.split())

file.close()

json_file=open("part.json","w")


json_file.write('''
    "LocalData": {
        "NetworkObjects": {
            "JsonStoreArray": [
                {
                    "Name": "%s",
                    "Type": 2,
                    "Uid": "{%s}"
                }
''' % ( defclassname , defclass_uuid  ) )

for comp in maclist:
    json_file.write('''
                    '
                    {
                        "HostAddress": "%s",
                        "MacAddress": "%s",
                        "Name": "%s",
                        "ParentUid": "%s",
                        "Type": 3,
                        "Uid": "%s"
                    }
    ''' % (comp[0] ,comp[1] ,comp[2] ,defclass_uuid ,uuid.uuid1() )    )


json_file.write('''
             ]
            }
        },
    ''')

json_file.close()
