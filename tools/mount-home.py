'''
在 5a88 電腦教室預設為自動還原，學生工作區使用 nas 分享，但同時共用，會造成誤存或拉動的問題。

寫了這個簡單程式掛載個人目錄到桌面上，簡化以上的問題。

設定部份：
會取得機器名稱做判斷，例如： F401 這樣的學生機器名稱。
再取 F4 的 nas 目錄是指向 \\nas2\F4Class 目錄中

執行：
建一個批次檔 bat，如下（都放在 c:\tools\ 目錄中）
python c:\tools\mount-home.py 

把批次檔 bat 做捷徑，註明要以系統管理員身份執行。

學生執行後會詢問班級代號，如：601 ，會檢查 \\nas2\F4Class 中有無 601 這個班級目錄（有再檢查學生機號的目錄，無則自動建立），就會連結為  MyHome_601_01 。
如果選擇取消，連結目錄會清除。重輸代號，會先自動刪除舊連結


'''
import socket ,os
import easygui

#設定 機器名  F4 開頭為 \\nas2\F4Class\ , F3 開頭為 \\nas2\F3Class\
class_path= {"F4":"\\\\nas2\\F4Class\\", "F3":"\\\\nas2\\F3Class\\"}
#連結目錄，要放在桌面
MyHomeRoot= "C:\\Users\\user\\Desktop\\"

#取得 機器名稱 等
hostname = socket.gethostname()
local_ip = socket.gethostbyname(hostname)

#id = local_ip.split('.')
#前2碼 教室代號
class_computer= hostname[0:2]
#第3碼以後 座號
sit_id = hostname[2:]

path_first = class_path.get(class_computer.upper())


for root, dirs, files in os.walk(MyHomeRoot):
  for dd in dirs:
    # print(dd)
    # 有舊連結目錄，先刪除
    if 'MyHome_' in dd :
      
      fullpath = os.path.join(root, dd)
      print(fullpath)
      os.rmdir(fullpath)




done = False
while not done :
  cid = easygui.enterbox('請輸入班級代號：（例六甲：601）')

  if cid == None :
    #放棄
    done = True
    continue

  try:

    chk_dir = "{}\\{}".format(path_first,cid)

    if   os.path.exists(chk_dir):
      # nas 會自動建立機號目錄
      if not os.path.exists(chk_dir + "\\{}".format(sit_id) ) :
        os.makedirs(  chk_dir+"\\{}".format(sit_id)  )

      #要掛載的命令
      cmd = "cmd /c mklink /D {3}MyHome_{0}_{1} {2}{0}\\{1}".format(cid,sit_id ,path_first ,MyHomeRoot)

      os.system(cmd)
      done = True
  except:
    continue

