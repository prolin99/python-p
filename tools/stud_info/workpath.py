"""在學生工作目錄中寫入一個 openid 文字檔
要有 openid excel 檔案


pip install pandas 
pip install openpyxl

"""
# 讀取 excel 
import pandas as pd
import os 

# 程式所在目錄
work_path = os.path.dirname(__file__)


df = pd.read_excel(work_path+'/openid.xlsx' ,engine='openpyxl')
#轉為串列格式
stud_list = df.to_numpy()


for y,c,sit,sid,name,openid,s6,s7,s8 in stud_list:
    if int(y)>0:
        y= int(y)
        c = int(c)
        sit = int(sit)
        sid = int(sid)
        if not os.path.exists( work_path + "/{0}{1:02}/".format(y,c)):
            os.makedirs(  work_path+"/{0}{1:02}".format(y,c)  )
        if not os.path.exists(work_path + "/{0}{1:02}/{2:02}".format(y,c,sit )):
            os.makedirs(  work_path+"/{0}{1:02}/{2:02}".format(y,c,sit )  )
        os.mknod( work_path + "/{0}{1:02}/{2:02}/{4}-{5}.txt".format(y,c ,sit,sid,name,openid))
        print("{0}{1:02}/{2:02}/{4}-{5}.txt".format(y,c ,sit,sid,name,openid) )
