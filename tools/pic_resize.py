#縮圖為 100*100 檔案+t.jpg
#pip install python-resize-image

import os

from PIL import Image
from resizeimage import resizeimage

path = "/home/user/Download/images/"
tu_path = "/home/user/Download/images/t/"
for i in os.listdir(path):
    if i.endswith((".png",".jpg","jpeg")):
        #縮圖檔名
        sfn = i.split('.')[0]  +'t.jpg'

        fd_img = open(path +i , 'rb')
        img = Image.open(fd_img)
        img = resizeimage.resize_thumbnail(img, [80, 80])

        img.save( tu_path + sfn , img.format)
        fd_img.close()
