# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1 ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"合併目錄中的 PDF", pos = wx.DefaultPosition, size = wx.Size( 473,429 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"PDF 所在目錄：", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )

		bSizer3.Add( self.m_staticText1, 0, wx.ALL, 5 )

		self.pdfpath = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.Size( 400,-1 ), wx.DIRP_DEFAULT_STYLE )
		bSizer3.Add( self.pdfpath, 0, wx.ALL, 5 )

		self.chk_bm_fg = wx.CheckBox( self, wx.ID_ANY, u"建書簽", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.chk_bm_fg.SetValue(True)
		bSizer3.Add( self.chk_bm_fg, 0, wx.ALL, 5 )

		self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"前頭幾字要去除", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )

		bSizer3.Add( self.m_staticText2, 0, wx.ALL, 5 )

		self.txt_beg = wx.TextCtrl( self, wx.ID_ANY, u"3", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.txt_beg, 0, wx.ALL, 5 )

		self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"要取代成空白的字串", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		bSizer3.Add( self.m_staticText3, 0, wx.ALL, 5 )

		self.txt_rep = wx.TextCtrl( self, wx.ID_ANY, u"_ok", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.txt_rep, 0, wx.ALL, 5 )

		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"存檔位置", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )

		bSizer3.Add( self.m_staticText5, 0, wx.ALL, 5 )

		self.save_path = wx.DirPickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.Size( 400,-1 ), wx.DIRP_DEFAULT_STYLE )
		bSizer3.Add( self.save_path, 0, wx.ALL, 5 )

		self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"產生的新檔名：", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		bSizer3.Add( self.m_staticText4, 0, wx.ALL, 5 )

		self.txt_newname = wx.TextCtrl( self, wx.ID_ANY, u"newpdf.pdf", wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
		bSizer3.Add( self.txt_newname, 0, wx.ALL, 5 )


		bSizer2.Add( bSizer3, 1, wx.EXPAND, 5 )

		self.btn_join = wx.Button( self, wx.ID_ANY, u"合併 PDF", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.btn_join, 0, wx.ALL, 5 )


		self.SetSizer( bSizer2 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.pdfpath.Bind( wx.EVT_DIRPICKER_CHANGED, self.path_get )
		self.btn_join.Bind( wx.EVT_BUTTON, self.do_join )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def path_get( self, event ):
		event.Skip()

	def do_join( self, event ):
		event.Skip()


