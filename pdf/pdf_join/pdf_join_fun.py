import os
import PyPDF2


def do_join_work(path , bookmake_fg  , save_path ,  save_pdf_name , beg , rep):
    # 獲取當前工作目錄
    #path = os.getcwd()
    #path = "C:/my-python/tmp/001"
    BookMark_page=[0]

    # 獲取目錄中所有文件列表
    files = os.listdir(path)

    # 過濾出 PDF 文件
    pdfFiles = filter(lambda filename: filename.endswith('.pdf'), files)

    # 打印 PDF 文件列表
    #for pdfFile in pdfFiles:
    #    print(pdfFile)
    pdfFiles_list= list(pdfFiles)

    pdfFiles_list_s = sorted(pdfFiles_list)




    # 合併成一個 PDF --------------------------------
    print("合併成一個 PDF --------------------------------")
    # 創建新 PDF 文件
    pdfWriter = PyPDF2.PdfWriter()

    mark_page = 0
    for file in pdfFiles_list:
        # 打開 PDF 文件
        file = path + '/' + file
        #print('---' ,  file)
        pdfFile = open(file, 'rb')
        pdfReader = PyPDF2.PdfReader(pdfFile)

        #記錄每一個檔案的頁數
        mark_page = mark_page+ len(pdfReader.pages)
        BookMark_page.append( mark_page )

        # 循環頁面，將每個頁面添加到新 PDF 文件中
        for pageNum in range(len(pdfReader.pages)):
            pdfWriter.add_page(pdfReader.pages[pageNum])

        # 關閉 PDF 文件
        pdfFile.close()

    # 保存新 PDF 文件
    pdfOutput = open(save_path  + '/' + save_pdf_name , 'wb')
    pdfWriter.write(pdfOutput)


    # 關閉所有文件
    pdfOutput.close()

    if (bookmake_fg):
        "#加入畫簽 -----------------------------------"
        #print("#加入畫簽 ------------")
        # 打開舊 PDF 文件
        pdfFile = open(save_path   + '/' + save_pdf_name, 'rb')
        pdfReader = PyPDF2.PdfReader(pdfFile)

        # 創建新 PDF 文件
        pdfWriter = PyPDF2.PdfWriter()

        # 添加目錄到新 PDF 文件
        i = 0 

        for BookMark_name in pdfFiles_list:
            BookMark_name = BookMark_name[beg:-4]
            BookMark_name = BookMark_name.replace(rep, "")
            print(BookMark_name)
            pdfWriter.add_outline_item(BookMark_name, BookMark_page[i])
            #print('====' , BookMark_name, BookMark_page[i])
            i = i+1



        # 從舊 PDF 文件循環頁面，將每個頁面添加到新 PDF 文件中
        for pageNum in range(len(pdfReader.pages)):
            pdfWriter.add_page(pdfReader.pages[pageNum])

        # 保存新 PDF 文件
        pdfOutput = open( save_path   + '/書簽-' + save_pdf_name , 'wb')
        pdfWriter.write(pdfOutput)

        # 關閉所有文件
        pdfOutput.close()
        pdfFile.close()
