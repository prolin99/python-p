import os
import wx
import wxGUI
import pdf_join_fun

class GuiFrame(wxGUI.MyFrame1):
    def __init__(self,parent):
        wxGUI.MyFrame1.__init__(self, parent)
        self.filename=''

    def f_close(self,event):
        self.Destroy()


    def path_get(self,event):
        #print(self.pdfpath.GetPath() )
        last_directory = os.path.basename(self.pdfpath.GetPath())
        self.txt_newname.SetValue(last_directory + '.pdf')
        #wx.MessageBox(f"Selected directory:{last_directory}", "Info", wx.OK | wx.ICON_INFORMATION)

    def do_join(self,event):
        path = self.pdfpath.GetPath()
        bookmake_fg = self.chk_bm_fg.IsChecked
        save_path = self.save_path.GetPath()
        save_pdf_name = self.txt_newname.GetValue()
        beg = int(self.txt_beg.GetValue())
        rep = self.txt_rep.GetValue()
        print(path ,bookmake_fg , save_path ,  save_pdf_name , beg , rep )

        pdf_join_fun.do_join_work(path ,bookmake_fg , save_path ,  save_pdf_name , beg , rep )
        wx.MessageBox(f"合併檔案完成:{save_pdf_name}", "Info", wx.OK | wx.ICON_INFORMATION)




if __name__ =='__main__':
    app = wx.App()
    frm = GuiFrame(None)
    frm.Show()
    app.MainLoop()
