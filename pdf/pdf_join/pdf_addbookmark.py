import PyPDF2

path = "C:/my-python/tmp/"

# 打開舊 PDF 文件
pdfFile = open(path + '000.pdf', 'rb')
pdfReader = PyPDF2.PdfReader(pdfFile)

# 創建新 PDF 文件
pdfWriter = PyPDF2.PdfWriter()

# 添加目錄到新 PDF 文件
pdfWriter.add_outline_item('Chapter 1', 0)
pdfWriter.add_outline_item('Chapter 2', 2)
pdfWriter.add_outline_item('Chapter 3', 4)

# 從舊 PDF 文件循環頁面，將每個頁面添加到新 PDF 文件中
for pageNum in range(len(pdfReader.pages)):
    pdfWriter.add_page(pdfReader.pages[pageNum])

# 保存新 PDF 文件
pdfOutput = open( path + 'new_file.pdf', 'wb')
pdfWriter.write(pdfOutput)

# 關閉所有文件
pdfOutput.close()
pdfFile.close()
