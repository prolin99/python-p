import fitz

# 將 "input.docx" 轉換成 "output.pdf"
doc = fitz.open("/home/user/git/python-p/pdf/doc2pdf/123.docx")
pdf_bytes = doc.convert_to_pdf()

with open("/home/user/git/python-p/pdf/doc2pdf/123.pdf", "wb") as f:
    f.write(pdf_bytes)
