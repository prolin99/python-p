import wx

class MyFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="Drag and Drop List", size=(300, 300))
        
        # 創建一個列表控件，支持多選和可拖曳排序
        self.list_ctrl = wx.ListCtrl(self, style=wx.LC_REPORT|wx.LC_SINGLE_SEL|wx.LC_EDIT_LABELS|wx.LC_SORT_ASCENDING)
        self.list_ctrl.InsertColumn(0, "Items")
        self.list_ctrl.SetColumnWidth(0, 220)
        
        # 將一些示例項目添加到列表中
        self.list_ctrl.InsertItem(0, "Item 1")
        self.list_ctrl.SetItemData(0, 1)
        self.list_ctrl.InsertItem(1, "Item 2")
        self.list_ctrl.SetItemData(1, 2)
        self.list_ctrl.InsertItem(2, "Item 3")
        self.list_ctrl.SetItemData(2, 3)
        
        # 為列表控件添加事件處理函數
        self.list_ctrl.Bind(wx.EVT_LIST_BEGIN_DRAG, self.on_begin_drag)
        self.list_ctrl.Bind(wx.EVT_LIST_END_DRAG, self.on_end_drag)
        
        # 將列表控件添加到主窗口中
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.list_ctrl, 1, wx.EXPAND|wx.ALL, 10)
        self.SetSizer(sizer)
    
    def on_begin_drag(self, event):
        # 當開始拖動列表項時，將所選項目的索引保存到屬性中
        self.drag_index = event.GetIndex()
    
    def on_end_drag(self, event):
        # 當結束拖動列表項時，獲取拖動的目標索引，並將項目插入到該位置
        target_index = event.GetIndex()
        if target_index != wx.NOT_FOUND and target_index != self.drag_index:
            text = self.list_ctrl.GetItemText(self.drag_index)
            data = self.list_ctrl.GetItemData(self.drag_index)
            self.list_ctrl.DeleteItem(self.drag_index)
            self.list_ctrl.InsertItem(target_index, text)
            self.list_ctrl.SetItemData(target_index, data)

app = wx.App()
frame = MyFrame()
frame.Show()
app.MainLoop()

