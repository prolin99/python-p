from __future__ import unicode_literals
import youtube_dl
import os , os.path

ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
}
with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    print(os.path.dirname(__file__) )
    
    with open( os.path.dirname(__file__) + '/mp3_url.txt') as url_file:
        for url_line in url_file:
            print(url_line)
            ydl.download([url_line])
