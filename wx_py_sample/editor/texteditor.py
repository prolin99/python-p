import wx
import wxGUI


class GuiFrame(wxGUI.MyFrame1):
    def __init__(self,parent):
        wxGUI.MyFrame1.__init__(self, parent)
        self.filename=''

    def f_close(self,event):
        self.Destroy()


    def f_open(self,event):
        dlg = wx.FileDialog(self, "Choose a file", self.filename, "", "Python files (*.py)|*.py", wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            #self.filename = dlg.GetFilename()
            #self.filename = dlg.GetDirectory()
            self.filename =dlg.GetPath()
            with open(self.filename, 'r') as f:
                contents = f.read()
                self.m_textCtrl1.SetValue(contents)

            dlg.Destroy()
            frm.SetTitle( 'text editor--' + self.filename)

    def f_save(self,event):
        if self.filename:
            with open(self.filename, 'w') as f :
                contests = self.m_textCtrl1.GetValue()
                f.write(contests)
        else:
            self.f_seveAs(self)

    def f_seveAs(self, event):
        #print('save as...')
        dlg = wx.FileDialog(self, "Input setting file path", "", "", "Python files(*.py)|*.*", wx.FD_SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            self.filename = dlg.GetPath()
            with open(self.filename, "w") as f:
                contests = self.m_textCtrl1.GetValue()
                f.write(contests)
                frm.SetTitle( 'text editor--' + self.filename)
            dlg.Destroy()


if __name__ =='__main__':
    app = wx.App()
    frm = GuiFrame(None)
    frm.Show()
    app.MainLoop()