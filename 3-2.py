#陣列可以修改
shoplist=['apple','milk','pen']
print(shoplist)
print(shoplist[0])
print(len(shoplist))

shoplist[2]='eggs'
print(shoplist)

print(shoplist.index('milk'))

#在後方增加
shoplist.append('bread')

print(shoplist)

#增加在前方
shoplist.insert(0,'butter')
print(shoplist)

#移出最後一個
buy =shoplist.pop()
print(buy)
print(shoplist)

#移除第二個 （0，1...）
shoplist.pop(1)
print(shoplist)

#排序
shoplist.sort()
print(shoplist)


for item in shoplist:
    print(item)

#陣列相加
newlist=['paper','ox']
newshoplist=shoplist+newlist
print(newshoplist)
