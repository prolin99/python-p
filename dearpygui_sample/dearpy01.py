from dearpygui.core import *
from dearpygui.simple import *

import os.path
py_path = os.path.dirname(__file__)

def save_callback(sender, data):
    print("中文")

with window("Example Window"):
    

    add_additional_font(py_path+'/STKaiTi.ttf', 18, glyph_ranges='chinese_full')

    add_text("Hello, Dear PyGui 中文字型")
    add_button("Save", callback=save_callback)
    add_input_text("string", default_value="Quick brown fox")
    add_slider_float("float", default_value=0.283, max_value=1)

start_dearpygui()
