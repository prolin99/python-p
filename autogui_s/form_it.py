import webbrowser
import time 
import pyautogui
import pyperclip

#使用 tab 在網頁中定位，但可能因輸入法等因素造成錯誤


webbrowser.open('https://docs.google.com/forms/d/e/1FAIpQLSd6k-VO-qvG138G7spFvSxOjrk6rBBhGo-5p6NaiouQkBJypw/viewform?usp=sf_link')


time.sleep(2)

#點到網頁
pyautogui.click(200,200)

#第一個輸入點  姓名
pyautogui.press('\t')

pyautogui.typewrite('Alice' + '\t')
time.sleep(0.5)

# 第二個輸入點選擇 -->選擇2
pyautogui.press(['down','\t','\t'])
time.sleep(0.5)
# 選擇第一個時 
#pyautogui.press([ 'space','\t' ,'\t'])
#time.sleep(0.5)

# 第三個輸入點 下拉 -->選擇第一個
pyautogui.press('down')
time.sleep(0.5)
pyautogui.press( 'space' ) 
time.sleep(0.5)
pyautogui.press('\t') 
time.sleep(0.5)

#說明文字 中文需要使用複製貼上功能
pyperclip.copy('中文字訊息')

pyautogui.hotkey('ctrl', 'v')
pyautogui.press('\t') 
time.sleep(0.5)

# 英文字可直接上字
#pyautogui.typewrite('no detail'  + '\t')

#填寫完成，確定鍵
pyautogui.press('enter')