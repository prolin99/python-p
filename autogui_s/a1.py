import pyautogui 
#每個 pyautogui 函數會間隔一秒
pyautogui.PAUSE =1
#程式錯誤時自動停止
pyautogui.FAILSAFE = True

#取得螢幕解析度
width, height = pyautogui.size()
print('width:{} ,height:{}'.format(width, height ))

#滑鼠移動矩形
for i in range(3):
    pyautogui.moveTo(100,100 , duration=0.25)
    pyautogui.moveTo(500,100 , duration=0.25)
    pyautogui.moveTo(500,500 , duration=0.25)
    pyautogui.moveTo(100,500 , duration=0.25)

#滑鼠右鍵
pyautogui.click(200,500, button='right')

print(pyautogui.position())