import requests

headers= {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'
}

res = requests.get('http://bj.xiaozhu.com/' , headers=headers)#網站為小豬短租網北京地區網址
#print(res)
#pycharm中返回結果為<Response [200]>，說明請求網址成功，若為404,400則請求網址失敗
try:
    print(res.text)
except  ConnectionError:
    print('拒連接')
#pycharm部分結果如圖3.4所示
