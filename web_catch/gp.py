import requests
from selenium.webdriver.support.ui import WebDriverWait as wait
from bs4 import BeautifulSoup
import time
import json

headers = {
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
}
s = requests.Session()
url = 'https://photos.google.com/share/AF1QipPbcZwZX1hti71LrVtUYcm0oH_CBUSKaE7Q43FpyQKOlcTTAWHvqqElZAN36QpHxQ?key=a091Z0c2WEJYV2RhSkNQS3pOYzA2aml5QXNhZGN3'

r = s.get(url ,   headers=headers)
r.arender()

soup = BeautifulSoup(r.text , 'html.parser')
imgs = soup.select('a')
i = 0 
for img in imgs:
    i += 1 
    print('----' , i )
    print(img['href'])

#page = json.loads(r.text[16:]) #skip some chars that throw json off

'''
headers = {
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
}
res = requests.get('', headers=headers)
# print(res.text)
time.sleep(5)
soup = BeautifulSoup(res.text, 'html.parser')
imgs = soup.select('a')
i = 0 
for img in imgs:
    i += 1 
    print('----' , i )
    print(img['href'])
'''