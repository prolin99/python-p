# 下載合適的 geckodriver
# http://chromedriver.storage.googleapis.com/index.html chrome 要配合chrome 版本
# https://github.com/mozilla/geckodriver/releases  Firefox 

from selenium import webdriver 

import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC


opts = Options()
#opts.headless = False

opts.add_argument("start-maximized")
opts.add_argument("disable-infobars")
opts.add_argument("--disable-extensions")
opts.add_argument("start-maximized")

opts.add_experimental_option("excludeSwitches", ["enable-automation"])
opts.add_experimental_option('useAutomationExtension', False)

browser = webdriver.Chrome(options=opts,executable_path='/usr/bin/chromedriver')
#browser = webdriver.Firefox()
browser.get('http://tainan.cloudhr.tw/TN_SCHOOL/login.aspx')
user="prolin"
pwd="##"

# 選用 openid 登入
linkElem = browser.find_element_by_name('OAuth_Button')
time.sleep(3.5)
linkElem.click()

# openID 帳密輸入
time.sleep(1)
userElem = browser.find_element_by_id('ctl00_Main_login1_UserName')
for i in range(len(user)):
    userElem.send_keys(user[i])
    time.sleep(0.2+i*0.2)
    #userElem.send_keys('prolin')
userElem.send_keys(Keys.TAB)

time.sleep(3.2)
pwdElem = browser.find_element_by_id('ctl00_Main_login1_Password')
for i in range(len(pwd)):
    pwdElem.send_keys(pwd[i])
    time.sleep(0.22+i*0.2)
#pwdElem.send_keys('cha1852##')
time.sleep(0.5)
pwdElem.send_keys(Keys.ENTER)
'''
time.sleep(3.4)
linkElem = browser.find_element_by_id('ctl00_Main_login1_LoginButton')
#linkElem.click()
linkElem.submit()
'''
# openID 確認身份 yes
time.sleep(1.4)
linkElem = browser.find_element_by_id('ctl00_Main_yes_button')
linkElem.click()


# 簽到
time.sleep(3)
#切換到 Frame 中 <frame src="main_01.aspx?Count=5574" name="main">
browser.switch_to.frame("main")
# 等待
wait(browser, 10).until(EC.element_to_be_clickable((By.ID, "SignUC1_btn_In"))).click()

