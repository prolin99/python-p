import requests
from bs4 import BeautifulSoup

s = requests.session()
s.headers.update({"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})

URL = "https://www.google.dk/search"
images = []

def get_images(query, start):
    screen_width = 1920
    screen_height = 1080
    params = {
        "q": query,
        "sa": "X",

        "ijn": start/100,
        "start": start,
        #"ei": "" - This seems like a unique ID, you might want to use it to avoid getting banned. But you probably still are.
    }

    request = s.get(URL, params=params )
    bs = BeautifulSoup(request.text , 'html.parser')
    print(bs.select('img'))

    for img in bs.findAll("div", {"class": "rg_di"}):
        images.append(img.find("img").attrs['data-src'])


#Will get 400 images.
for x in range(0, 1):
    get_images("cats", x*100)

for x in images:
    print (x)