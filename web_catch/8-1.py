import time
from multiprocessing import Pool

def re_scraper(url):
    print(url)
    time.sleep(0.2)
    
    return

if __name__== '__main__':
    urls=['https://www.syps.tn.edu/start={}'.format(str(i)) for i in range(0,250,25)]
    t1= time.time()
    for url in urls:
        re_scraper(url)
    e1= time.time()
    print(e1-t1)

    t2= time.time()
    pool = Pool(processes=2)
    pool.map(re_scraper, urls )
    e2= time.time()
    print(e2-t2)
