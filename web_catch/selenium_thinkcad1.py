# 下載合適的 geckodriver
# http://chromedriver.storage.googleapis.com/index.html chrome 要配合chrome 版本
# https://github.com/mozilla/geckodriver/releases  Firefox 

# 讀取學生資料
import os
import pandas as pd
df = pd.read_excel(os.path.dirname(__file__)+'/openid.xlsx')


# web 控制
from selenium import webdriver

import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC


opts = Options()
#opts.headless = False

opts.add_argument("start-maximized")
opts.add_argument("disable-infobars")
opts.add_argument("--disable-extensions")
opts.add_argument("start-maximized")

opts.add_experimental_option("excludeSwitches", ["enable-automation"])
opts.add_experimental_option('useAutomationExtension', False)

browser = webdriver.Chrome(options=opts,executable_path='/usr/bin/chromedriver')
#browser = webdriver.Firefox()
browser.get('https://www.tinkercad.com/')

#time.sleep(40)
# 等到有 加入學生  按鍵出現
wait(browser, 120).until(EC.element_to_be_clickable((By.ID, "addStudentModalButton"))).click()


#time.sleep(5)
#browser.switch_to.frame(1)
#browser.switch_to.frame(browser.find_elements_by_tag_name("iframe")[0])

for row in range(df.index.stop):
    #第一班
    if (df.values[row][1] == 2 ) :
        # add one
        #userElem=browser.switch_to.active_element
        time.sleep(3)
        userElem = browser.find_element_by_id('first-last-name')
        userElem.send_keys('{}{:02d}{:02d}{}'.format(df.values[row][0],df.values[row][1],df.values[row][2], df.values[row][4]) )

        time.sleep(2)
        Elem = browser.find_element_by_id('identifier')
        Elem.clear()
        Elem.send_keys(df.values[row][5])

        time.sleep(1)
        wait(browser, 10).until(EC.element_to_be_clickable((By.ID, "addStudentSaveStudentButton"))).click()
