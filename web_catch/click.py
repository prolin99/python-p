import requests , bs4

try:
    res = requests.get('http://www.haodoo.net/?M=book&P=505')
    res.raise_for_status()

    soup = bs4.BeautifulSoup(res.text , "html.parser")
    # 取得書下載連結
    elems = soup.select('input[value="下載 epub 檔"]')
    print(elems)
    print(elems[0].get('onclick'))
    book = (elems[0].get('onclick').split("'")[1] )+ '.epub'

    # 分析 d.js
    # DownloadEpub 會導向
    # http://www.haodoo.net/?M=d&P=A505.epub

    book_url = 'http://www.haodoo.net/?M=d&P='+  book
    print(book_url)



    # 取完整中文書名
    elems = soup.select('table > tr > td > script')
    print(elems[0].getText().split('\r\n') )
    ss = elems[0].getText().split('\r\n')[1]
    book_name= ss.split('"')[1] + '.epub'
    print(book_name)

    # 下載存檔

    r = requests.get( book_url , stream=True)
    r.raise_for_status()


    with open(book_name, 'wb') as f:
        # 大檔案使用 iter_content 
        for chunk in r.iter_content(100000):
            f.write(chunk)





except Exception as exc:
    print('There was a problem: {}'.format(exc))