import requests, os, bs4 , time

url = 'https://xkcd.com'
os.makedirs('xkcd', exist_ok = True)
try:
    while not url.endswith('#'):
        print('Downloading page {}'.format(url))
        res = requests.get(url)
        #如果網路取得有問題，會產生異常
        res.raise_for_status()


        soup = bs4.BeautifulSoup(res.text , "html.parser")

        # <a rel="prev" href="/863/" accesskey="p">&lt; Prev</a>
        # 取得位置
        prevLink = soup.select('a[rel="prev"]')[0]
        url = 'https://xkcd.com' + prevLink.get('href')


        #<img src="//imgs.xkcd.com/comics/flying_cars.png" title="It's hard to fit in the backseat of my flying car with my android Realdoll when we're both wearing jetpacks." alt="Flying Cars" style="image-orientation:none">
        # 下載圖檔
        comic_lst = soup.select('#comic img')
        for image in comic_lst:
            comicUrl = 'https:' +  image.get('src')
            #comicUrl = 'https:' +  image['src']    #也可 image['src']

            print(comicUrl)
            #圖檔 stream=True 可以避免立即將大的 response 內容讀入記憶體內
            r = requests.get(comicUrl , stream=True)
            r.raise_for_status()

            imageFile = os.path.join('xkcd', comicUrl.split("/")[-1] )

            with open(imageFile, 'wb') as f:
                # 大檔案使用 iter_content 
                for chunk in r.iter_content(100000):
                    f.write(chunk)


        time.sleep(2)

except Exception as exc:
    print('There was a problem: {}'.format(exc))



print('Done')

