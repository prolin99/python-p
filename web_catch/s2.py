# 下載合適的 geckodriver
# http://chromedriver.storage.googleapis.com/index.html chrome 要配合chrome 版本
# https://github.com/mozilla/geckodriver/releases  Firefox 


# web 控制
from selenium import webdriver

import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC

from bs4 import BeautifulSoup
from urllib.parse import urljoin

opts = Options()
#opts.headless = False

opts.add_argument("start-maximized")
opts.add_argument("disable-infobars")
opts.add_argument("--disable-extensions")
opts.add_argument("start-maximized")

opts.add_experimental_option("excludeSwitches", ["enable-automation"])
opts.add_experimental_option('useAutomationExtension', False)

browser = webdriver.Chrome(options=opts,executable_path='/usr/bin/chromedriver')

url = 'https://photos.google.com/share/AF1QipPbcZwZX1hti71LrVtUYcm0oH_CBUSKaE7Q43FpyQKOlcTTAWHvqqElZAN36QpHxQ?key=a091Z0c2WEJYV2RhSkNQS3pOYzA2aml5QXNhZGN3'

browser.get(url)


html = browser.find_element_by_tag_name('html')
i = 0

for i in range(20):
    html.send_keys(Keys.PAGE_DOWN)

    soup = BeautifulSoup(browser.page_source, "html.parser")
    urls = soup.select('a')
    print(len(urls))
    time.sleep(1.5)
'''
elems = browser.find_elements_by_xpath("//*[@href]")

for elem in elems:
    i += 1
    print(i , elem.get_attribute("href"))

#urls = []
soup = BeautifulSoup(browser.page_source, "html.parser")
urls = soup.select('a')
print(len(urls))
#print(urls['href'])
'''