#pool
import multiprocessing as mp

def job(x):
    res = 0
    for _ in range(x):
        for i in range(1000000):
            res += i + i**2 + i**3
    return res

def multicore():
    #指定使用幾個核心，略去代表 使用全部核心
    #pool = mp.Pool(processes=2)
    pool = mp.Pool()
    #自動配給各個核心
    res = pool.map(job ,range(10))
    print(res)

    #只使用一個核心，但只能傳一個參數
    res=pool.apply_async(job, (10,))
    #取值方式
    print(res.get())

    # 迭代器，i=0时apply一次，i=1时apply一次等等
    multi_res = [pool.apply_async(job, (i,)) for i in range(10)]
    # 从迭代器中取出
    print([res.get() for res in multi_res])

if __name__ == '__main__'    :
    multicore()
