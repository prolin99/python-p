import multiprocessing as mp

import time
def job(v,num,l ):
    #鎖定
    l.acquire()
    for _ in range(10):
        time.sleep(0.1)
        v.value += num
        print(v.value  )
    #完成 解鎖
    l.release()

def multicore():
    #lock 鎖定
    l =mp.Lock()

    #多核共用變數
    v = mp.Value('i',0)

    p1 = mp.Process(target=job, args=(v,1,l))
    p2 = mp.Process(target=job, args=(v,3,l))
    p1.start()
    p2.start()
    p1.join()
    p2.join()

if __name__ == '__main__':
    multicore()
