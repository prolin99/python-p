#使用多核心，無使用到多緒
#  pool 方式，自動處理多核心分程，可以取得傳回值
import multiprocessing.pool

def get_it(item):
    res=0
    for i in range(100):
        print(item*100+i)
        res += item*100+i
    return res

pool = multiprocessing.pool.Pool()
res = pool.map(get_it, range(10))
#也可以指定持定參數
res = pool.map(get_it, [1,4,5,6])

print(res)
