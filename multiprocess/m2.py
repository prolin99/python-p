import multiprocessing as mp
import threading as td
import time

def job(q):
    res=0
    for i in range(1000000):
        res += i + i**2 + i**3
    q.put(res)
    #多核無法有返回值，所以放在 Queue 中，再取出

#多核
def multcore():

    q= mp.Queue()
    #t1 = td.Tread(target=job , args=(1,2) )
    p1= mp.Process(target=job , args=(q,))
    p2= mp.Process(target=job , args=(q,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()

    res1=q.get()
    res2=q.get()
    print(res1+ res2 )

#多執行緒
def multithread():
    q= mp.Queue()
    t1=td.Thread(target=job, args=(q,))
    t2=td.Thread(target=job, args=(q,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()

    res1=q.get()
    res2=q.get()
    print(res1+ res2)

#一般
def normal():
    res=0
    for _ in range(2):
        for i in range(1000000):
            res += i + i**2 + i**3
    print('normal' , res)

if __name__ =='__main__':
    #一般
    print('normal:')
    st=time.time()
    normal()
    st1=time.time()
    print('normal:', st1-st)

    #多核
    multcore()
    st2=time.time()
    print('multcore' , st2-st1)

    #多緒
    multithread()
    st3= time.time()
    print('multithread', st3-st2)
