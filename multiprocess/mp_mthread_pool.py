#多核及多緒
import os


#from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

def get_it(item):
    res=0
    for i in range(100):
        print(item*100+i)
        res += item*100+i
    return res

if __name__ == '__main__':

    val = [1,3,6,9,12]

    pool = ThreadPool(4)
    res=pool.map(get_it,val)
    pool.close()
    pool.join()

    print(res)
