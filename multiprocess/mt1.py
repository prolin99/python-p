#多核
import multiprocessing as mp

import threading as td

def job(q,max):
    res=0
    for i in range(max):
        res += i + i**2 + i**3
    q.put(res)




if __name__ == '__main__':
    #多核無法有返回值，所以放在 Queue 中，再取出
    q= mp.Queue()
    #t1 = td.Tread(target=job , args=(1,2) )
    p1= mp.Process(target=job , args=(q,1000000))
    p2= mp.Process(target=job , args=(q,10000000))
    p1.start()
    p2.start()
    p1.join()
    p2.join()

    res1=q.get()
    res2=q.get()
    print(res1, res2 )
