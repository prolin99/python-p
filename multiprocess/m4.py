#共享 memory
import multiprocessing as mp

#多核共用值
value = mp.Value('d',1)

#只有一維資料
array = mp.Array('i',[1,3,4])
