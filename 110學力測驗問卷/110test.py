"""在學生工作目錄中寫入一個 openid 文字檔
要有 openid excel 檔案


pip install pandas 
pip install openpyxl

"""
# 讀取 excel 
import pandas as pd
import os 

# 程式所在目錄
work_path = os.path.dirname(__file__) 


df = pd.read_excel(work_path+'/110.xlsx' ,engine='openpyxl')
#轉為串列格式
stud_list = df.to_numpy()


for sy,sno,s,name ,y,c,sit,pwd,done in stud_list:
    #print(sy,sno,s,name,y,c,sit,pwd, done)
    if int(y)>0:
        y= int(y)
        c = int(c)
        oy = y
        oc = c
        if (int(sy)==110):
            y = y + 1
            c= c+100


        sit = int(sit)

        if not os.path.exists( work_path + "/{0:03}/".format(c)):
            os.makedirs(  work_path+"/{0:03}".format(c)  )
        if not os.path.exists(work_path + "/{1:03}/{2:02}".format(y,c,sit )):
            os.makedirs(  work_path+"/{1:03}/{2:02}".format(y,c,sit )  )
        #os.mkdir( work_path + "/{0}{1:02}/{2:02}/{4}-{5}.txt".format(y,c ,sit,sid,name,openid))
        stud_file_name =  work_path +  "/{1:03}/{2:02}/學力問卷-{3}.txt".format(y,c ,sit,name)
        with open(stud_file_name , 'w' , encoding='utf-8' ) as fp:
            doc = "{0} 學年度：{2} , 年級：{3} 班級：{4} 座號：{5}   密碼: {1:06}".format(name,int(pwd) ,int(sy), int(oy) , int(oc) ,int(sit)  )
            fp.write(doc)



        #print("{0}{1:02}/{2:02}/{4}-{5}.txt".format(y,c ,sit,sid,name,openid) )
