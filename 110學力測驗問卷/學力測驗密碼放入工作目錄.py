"""在學生工作目錄中寫入一個 openid 文字檔
要有 openid excel 檔案


pip install pandas 
pip install openpyxl
pip install easygui

"""
# 讀取 excel 
import pandas as pd
import os 
import easygui

# 程式所在目錄
now_path = os.path.dirname(__file__)
# if now_path /class_data 不存在，建立程式所在目錄下的 class_data 目錄
if not os.path.exists(now_path + "/class_data"):
    os.makedirs(now_path + "/class_data")
work_path = os.path.dirname(__file__) + '/class_data'

# easygui open a .xlsx file ， 預設目錄在程式所在目錄
file_path = easygui.fileopenbox(default="*.xlsx", filetypes=["*.xlsx"])
df = pd.read_excel(file_path, engine='openpyxl')


#轉為串列格式
stud_list = df.to_numpy()


for sno,s,name ,y,c,sit,pwd,done in stud_list:
    try:
        if int(y)>0:
            y= int(y)
            c = int(c)
            sit = int(sit)
            if sit > 0 :
                # 建立學生工作目錄
                if not os.path.exists( work_path + "/{0:03}/".format(c)):
                    os.makedirs(  work_path+"/{0:03}".format(c)  )
                if not os.path.exists(work_path + "/{1:03}/{2:02}".format(y,c,sit )):
                    os.makedirs(  work_path+"/{1:03}/{2:02}".format(y,c,sit )  )
                #os.mkdir( work_path + "/{0}{1:02}/{2:02}/{4}-{5}.txt".format(y,c ,sit,sid,name,openid))
                stud_file_name =  work_path +  "/{1:03}/{2:02}/{2:02}-{3}-學力問卷密碼.txt".format(y,c ,sit,name)
                with open(stud_file_name , 'w' , encoding='utf-8' ) as fp:
                    doc = "{0} 座號：{1}  學力問卷密碼: {2:06}".format(name, sit ,pwd)
                    fp.write(doc)
    except:
        continue

