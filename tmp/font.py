from PIL import Image, ImageDraw, ImageFont

def text_image(text, font_path, font_size, text_color):
    font = ImageFont.truetype(font_path, font_size)
    size = font.getsize(text)
    image = Image.new("RGBA", size, (255, 255, 255, 0))
    draw = ImageDraw.Draw(image)
    draw.text((0, 0), text, font=font, fill=text_color)
    return image


def read_one_chinese_char_per_line(file_path):
    with open(file_path, "r", encoding="utf-8") as file:
        for line in file:
            for text in line:
                print(text)
                font_path = "/home/user/git/python-p/tmp/g.ttf"
                font_size = 600
                text_color = (0, 0, 0)

                image = text_image(text, font_path, font_size, text_color)
                image.save(   "{}.png".format(text) )


file_path = "/home/user/git/python-p/tmp/text.txt"
read_one_chinese_char_per_line(file_path)




