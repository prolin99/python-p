#! /usr/bin/python3
# 文字檔 轉換成 epub
# 需要 python3-tk 套件
import sys ,os , zipfile ,shutil ,tempfile ,time
import re
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
#簡繁轉換模組
from opencc import OpenCC
#找文字檔編碼
import chardet


#所在程式目錄
ScriptPath = os.path.dirname(sys.argv[0])

#直接帶進文字檔參數
def_txt=''
if (len(sys.argv)>1):
    def_txt= sys.argv[1]

#print(ScriptPath)
#暫存區的
SavePath = tempfile.gettempdir()
#print(SavePath)

def unzip_epub():
    #解壓縮 epub
    with zipfile.ZipFile(ScriptPath + '/epub-src2.zip', 'r') as myzip:
        for file in myzip.namelist():
            myzip.extract(file,SavePath + '/epub')


html_beg='''<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-TW" xmlns:xml="http://www.w3.org/XML/1998/namespace">
<head>
  <!-- InstanceBeginEditable name="doctitle" -->

  <title>%s</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- InstanceEndEditable -->  <link href="../Styles/%s.css" rel="stylesheet" type="text/css"/>
<!-- InstanceBeginEditable name="head" -->  <!-- InstanceEndEditable -->
</head>

<body %s >
  <div>
    <div>
      <!-- InstanceBeginEditable name="Content" -->
'''


html_end='''
    </div>
<!--End content-->  </div>
<!--End ADE-->  <!-- InstanceEnd -->
</body>
</html>
'''

navpoint="""
              <navPoint id="navPoint-%d" playOrder="%d">
                <navLabel>
                  <text>%s</text>
                </navLabel>
                <content src="Text/ch%d.html"/>
              </navPoint>
"""

tocdoc="""<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN"
 "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd"><ncx version="2005-1" xmlns="http://www.daisy.org/z3986/2005/ncx/">
  <head>
    <meta content="urn:uuid:eba0e149-7241-43f3-a563-7b132597c3a5" name="dtb:uid"/>
    <meta content="1" name="dtb:depth"/>
    <meta content="0" name="dtb:totalPageCount"/>
    <meta content="0" name="dtb:maxPageNumber"/>
  </head>
  <docTitle>
    <text>%s</text>
  </docTitle>
  <navMap>
    %s
  </navMap>
</ncx>
"""

content_beg="""<?xml version="1.0" encoding="utf-8"?>
<package version="2.0" unique-identifier="BookID" xmlns="http://www.idpf.org/2007/opf">
  <metadata xmlns:opf="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/">
    <dc:title>%s</dc:title>
    <dc:creator opf:role="aut">%s</dc:creator>
    <dc:language>zh</dc:language>
    <dc:identifier opf:scheme="UUID" id="BookID">urn:uuid:8c7b74e5-d8c5-40e1-bf6f-55cbc5db9c1f</dc:identifier>
    <meta content="0.9.9" name="Sigil version" />
    <dc:date opf:event="modification">%s</dc:date>
  </metadata>
  <manifest>
    <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml"/>
    <item id="Style.css" href="Styles/Style.css" media-type="text/css"/>
    <item id="VStyle.css" href="Styles/VStyle.css" media-type="text/css"/>
    %s
  </manifest>
  <spine toc="ncx">
    %s
  </spine>
  <guide>
  </guide>
</package>
"""



#文字編碼方式
'''
def get_txt_encode(fn):
    #encodings 順序會造成正確性
    encodings = [  'utf-8',  'big5', 'GBK' ,'utf-16'  ]
    txt_code='big5'
    for e in encodings:
        try:
            fh = open(fn, 'r', encoding=e)
            fh.readlines()
            fh.seek(0)
        except UnicodeDecodeError:
            print('got unicode error with %s , trying different encoding' % e)
        else:
            txt_code = e
            print('opening the file with encoding:  %s ' % e)
            fh.close()
            break

    return txt_code
'''
def get_txt_encode(fn):
    rawdata = open(fn, "rb").read()
    result = chardet.detect(rawdata)
    charenc = result['encoding']
    #如無法偵測 預設 big5
    if charenc == None:
        charenc='Big5'
    print(charenc)
    return charenc

#寫入分吤章節 html
def savepage(page,doc):
    fn = SavePath + ('/epub/OEBPS/Text/ch%d.html' % page)
    file= open(fn,'w' ,encoding='utf8')
    file.write( doc + html_end)
    file.close()

def saveToc(navpoint):
    fn = SavePath + '/epub/OEBPS/toc.ncx'
    #print(fn)
    file= open(fn,'w', encoding='utf8')
    file.write(tocdoc % ('book',navpoint) )
    file.close()

def savecontent(book_name, auth_name, page):
    fn = SavePath + '/epub/OEBPS/content.opf'
    #print(fn)
    item=''
    itemref=''

    file= open(fn,'w', encoding='utf8')
    today= time.strftime("%Y-%m-%d", time.localtime())

    for i in range(101,page+1):
        item= item+ '   <item id="ch%d.html" href="Text/ch%d.html" media-type="application/xhtml+xml"/>\n' %(i,i)
        itemref=itemref + ' <itemref idref="ch%s.html"/>\n' % i

    file.write(content_beg % (book_name ,auth_name,today , item , itemref)  )
    file.close()



#讀取文字檔
def txt2html(fn ,book_name , auth_name  ,ts ,VH , decode ):

    page=101
    line_num=1
    if VH==1:
        style='Style'
        body_class=''
    else:
        style='VStyle'
        body_class=' class="vertical"'

    #print(body_class)
    html_doc= html_beg % ("首頁" ,style, body_class)

    nav = navpoint %(101,101,'首頁',101)

    try:

        #編碼

        if decode==3:
            dc='GBK'
            #file = open(fn,'r').read().decode("GBK").encode("utf8")
            file= open(fn,"rb")
        elif decode==2:
            dc='BIG5'
            #file = open(fn,'r').read().decode("big5").encode("utf8")
            file= open(fn,"rb")
        elif decode==4:

            dc=''
            file= open(fn,"r",encoding='utf-16')
        elif decode==5:
            #自動判別
            #dc=''
            dc = get_txt_encode(fn)
            file= open(fn,"rb")
        else:
            dc=''
            file= open(fn,"r",encoding='utf8')


        #簡繁轉換
        openCC = OpenCC()
        if ts == 2:
            #簡轉繁
            openCC.set_conversion('s2twp')
        if ts == 3:
            #繁轉簡
            openCC.set_conversion('tw2s')


        for line in file.readlines():
            if dc != '' :
                try:
                    #轉碼錯誤忽略
                    line=str(line.decode(dc ,'ignore') )
                except:
                    result=False
                    print( 'err-%s --%s' % (line_num , line) )

            #清除 \x00  不正常的 utf8 碼
            line = line.replace('\x00', '')

            if ts>1:
                try:
                    nline=''
                    nline = openCC.convert(line)
                    line=nline
                except:
                    print('簡繁- %s' % line )

            line = line.strip()  #移除頭尾特定字符如空白
            #print( 'ok-%s --%s' % (line_num , line) )
            line_num = line_num +1


            match = False
            if len(line)<30:
                #章節
                pattern = re.compile(r'第.+(卷|章|節)')
                # 取得匹配結果，無法匹配返回 None
                match = pattern.search(line)


            if  match or  (line_num > 500) :

                savepage(page , html_doc)
                page=page+1

                html_doc= html_beg % ("續頁" ,style, body_class)

                line_num =1

            if match:

                html_doc= html_beg %  (line , style,  body_class)
                html_doc = html_doc + '<h1 id="heading_id_2">%s</h1>\n' % line
                nav = nav + navpoint % (page, page, line, page)
            else:
                html_doc= html_doc + '<p>' +line  +'</p>\n'
            result = True
    except:
        result=False

    finally:
        print('end' )
        file.close()

    if result:
        savepage(page , html_doc)
        #章節選單
        if nav:
            saveToc(nav)
        #content
        savecontent(book_name, auth_name, page)

    return result

if __name__ == "__main__":

    win=tk.Tk()
    win.title("text to epub")
    win.geometry("500x500")
    txtfn=''
    book=''
    auth=''

    txtfn_v = tk.StringVar()
    txtfn_v.set('')

    book_v = tk.StringVar()
    book_v.set('')

    auth_v = tk.StringVar()
    auth_v.set('')

    txt_code = tk.IntVar()
    txt_code.set(5)

    ts_mode = tk.IntVar()
    ts_mode.set(1)

    txt_VH = tk.IntVar()
    txt_VH.set(1)

    FILEOPENOPTIONS = dict(defaultextension='.txt',
                       filetypes=[('文字檔','*.txt'), ('所有檔案','*.*')])

    def btnOpenClick():
        global txtfn
        txtfn = filedialog.askopenfilename(**FILEOPENOPTIONS)
        txtfn_v.set(txtfn)
        tmpepub_fn = os.path.split(txtfn)[1]
        #book_v.set(tmpepub_fn.split('.')[0] )
        book_v.set(tmpepub_fn[:-4])

    def btnClick():
        global txtfn
        book= book_v.get()
        auth= auth_v.get()
        decode= txt_code.get()

        ts=ts_mode.get()
        vh = txt_VH.get()

        #epub 架構檔
        unzip_epub()
        #開始轉換
        success = txt2html( txtfn ,book,auth  , ts ,vh , decode)

        if success :

            #壓縮 成 epub，存在同目錄
            newpubfn=book +'_'+ auth +'.epub'
            zf = zipfile.ZipFile(os.path.dirname(txtfn) + '/'+newpubfn , mode='w')#只儲存不壓縮
            os.chdir(SavePath +'/epub')
            for root, folders, files in os.walk("./"):
                for sfile in files:
                    aFile = os.path.join(root, sfile)
                    zf.write(aFile)
            zf.close()
            #label1.text=''
            txtfn=''
            book=''
            auth=''
            txtfn_v.set('')
            book_v.set('')
            auth_v.set('')

            #message
            messagebox.showinfo("txt2epub", "文字檔轉成 " +  newpubfn  +' 完成！')
        else:
            messagebox.showinfo("txt2epub", "轉檔失敗，請檢查編碼格式！")
        #移除工作檔
        shutil.rmtree(SavePath +'/epub' , ignore_errors=True)

    frame0 = tk.Frame(win)
    btnOpen = tk.Button(frame0, text='選擇文字檔' ,command=btnOpenClick).grid(row=0,column=0)
    #btnOpen.pack()

    #v.set('文字檔：')
    label1=tk.Label(frame0, width=30, textvariable=txtfn_v).grid(row=0,column=1)
    #label1.pack()
    frame0.pack()

    tk.Label(win, text="文字檔編碼：" ,bg='gray').pack(fill=tk.BOTH,pady=10)
    fram_select = tk.Frame(win  )
    #tk.Label(fram_select, text="文字檔編碼：").grid(row=0,column=0)
    tk.Radiobutton(fram_select,
              text="auto",
              padx = 20,
              variable=txt_code,
              value=5).grid(row=0,column=0)

    tk.Radiobutton(fram_select,
              text="UTF-8",
              padx = 20,
              variable=txt_code,
              value=1).grid(row=0,column=1)
    tk.Radiobutton(fram_select,
              text="BIG5",
              padx = 20,
              variable=txt_code,
              value=2).grid(row=0,column=2)
    tk.Radiobutton(fram_select,
              text="GBK",
              padx = 20,
              variable=txt_code,
              value=3).grid(row=1,column=0)

    tk.Radiobutton(fram_select,
              text="UTF-16",
              padx = 20,
              variable=txt_code,
              value=4).grid(row=1,column=1)
    fram_select.pack()

    tk.Label(win, text="簡繁轉換",bg='gray').pack(fill=tk.BOTH,pady=10)
    frame_ts = tk.Frame(win  )
    #tk.Label(frame_ts, text="簡繁轉換").grid(row=0,column=0)
    tk.Radiobutton(frame_ts,
              text="不轉換",
              padx = 20,
              variable=ts_mode,
              value=1).grid(row=0,column=0)
    tk.Radiobutton(frame_ts,
              text="簡轉繁Tw",
              padx = 20,
              variable=ts_mode,
              value=2).grid(row=0,column=1)
    tk.Radiobutton(frame_ts,
              text="繁轉簡",
              padx = 20,
              variable=ts_mode,
              value=3).grid(row=0,column=2)
    frame_ts.pack()

    tk.Label(win, text="文字走向",bg='gray').pack(fill=tk.BOTH,pady=10)
    fram_VH = tk.Frame(win)
    #tk.Label(fram_VH, text="文字走向").grid(row=0,column=0)
    tk.Radiobutton(fram_VH,
              text="橫向",
              padx = 20,
              variable=txt_VH,
              value=1).grid(row=0,column=0)
    tk.Radiobutton(fram_VH,
              text="直向",
              padx = 20,
              variable=txt_VH,
              value=2).grid(row=0,column=1)
    fram_VH.pack()


    frame1 = tk.Frame(win  )
    label2=tk.Label(frame1, text='書名：' ,height=2).grid(row=0,column=0)
    #label2.pack()
    edit1=tk.Entry(frame1 ,width=40 ,textvariable=book_v).grid(row=0,column=1)
    #edit1.pack()
    frame1.pack()

    frame2 = tk.Frame(win )
    label3=tk.Label(frame2, text='作者：' ,height=2).grid(row=0,column=0)
    #label3.pack()
    edit1=tk.Entry(frame2,width=40 ,textvariable=auth_v).grid(row=0,column=1)
    #edit1.pack()
    frame2.pack( )

    button1=tk.Button(win, text='開始轉換...' ,width=10 , command=btnClick )
    button1.pack(side='left',padx=(20,0), pady=(20,20))

    btnClose=tk.Button(win, text='結束' ,width=10, comman =win.destroy )
    btnClose.pack(side='left',padx=(20,0), pady=(20,20))

    #直接帶進文字檔
    if (def_txt != ''):
        txtfn=def_txt
        txtfn_v.set(def_txt)
        tmpepub_fn = os.path.split(def_txt)[1]
        #book_v.set(tmpepub_fn.split('.')[0] )
        book_v.set(tmpepub_fn[:-4] )
        def_txt=''

    win.mainloop()
