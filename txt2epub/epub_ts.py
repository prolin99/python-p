#! /usr/bin/python3
# 文字檔 轉換成 epub
# 需要 python3-tk 套件
import sys ,os , zipfile ,shutil ,tempfile ,time
import re ,random
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
#簡繁轉換模組
from opencc import OpenCC

#所在程式目錄
ScriptPath = os.path.dirname(sys.argv[0])
#print(ScriptPath)
#暫存區的
SavePath = tempfile.gettempdir()
tmpEpubPath = '/epub%s' % random.randint(1000,9999)
#print(SavePath)

openCC = OpenCC()
openCC.set_conversion('s2twp')

def unzip_epub( epubfn ):
    #解壓縮 epub
    with zipfile.ZipFile(epubfn, 'r') as myzip:
        for file in myzip.namelist():
            myzip.extract(file,SavePath + tmpEpubPath )


def file_Ts(fn):
    os.path.isfile
    file= open(fn,"r",encoding='utf8')
    data = file.read()
    file.close()
    data=openCC.convert(data)

    data= re.sub('xml:lang="zh-CN"','xml:lang="zh-TW"',data)
    data= re.sub('<dc:language>zh-cn</dc:language>','<dc:language>zh</dc:language>',data)

    file= open(fn,"w",encoding='utf8')
    file.write(data)
    file.close()
    #file

def path_ts_do(path):
    #xhtml  htm html opf ncx
    included_extenstions = ['xhtml', 'html', 'htm', 'opf' , 'ncx' ,'xml']
    # walk 遞迴取檔
    for root,d,f in os.walk(path):
        for file in f:
             if any(file.endswith(ext) for ext in included_extenstions):
                 #print( os.path.join(root,file) )
                 file_Ts(os.path.join(root,file))

    return True

if __name__ == "__main__":

    win=tk.Tk()
    win.title("epub 簡轉繁")
    txtfn=''
    book=''
    auth=''

    txtfn_v = tk.StringVar()
    txtfn_v.set('')

    book_v = tk.StringVar()
    book_v.set('')

    auth_v = tk.StringVar()
    auth_v.set('')



    FILEOPENOPTIONS = dict(defaultextension='.epub',
                       filetypes=[('電子書檔','*.epub'), ('所有檔案','*.*')])

    def btnOpenClick():
        global txtfn
        txtfn = filedialog.askopenfilename(**FILEOPENOPTIONS)
        txtfn_v.set(txtfn)
        tmpepub_fn = openCC.convert( os.path.split(txtfn)[1] )
        book_v.set(tmpepub_fn.split('.')[0] )

    def btnClick():
        global txtfn
        book= book_v.get()
        auth= auth_v.get()


        #解開 epub
        unzip_epub(txtfn)
        #開始轉換 簡轉繁
        success = path_ts_do(SavePath + tmpEpubPath )

        if success :

            #壓縮 成 epub，存在同目錄
            newpubfn=book +'_'+ auth +'.epub'
            zf = zipfile.ZipFile(os.path.dirname(txtfn) + '/'+newpubfn , mode='w')#只儲存不壓縮
            os.chdir(SavePath + tmpEpubPath)
            for root, folders, files in os.walk("./"):
                for sfile in files:
                    aFile = os.path.join(root, sfile)
                    zf.write(aFile)
            zf.close()
            #label1.text=''
            txtfn=''
            book=''
            auth=''
            txtfn_v.set('')
            book_v.set('')
            auth_v.set('')

            #message
            messagebox.showinfo("epub簡轉繁", "轉成 " +  newpubfn  +' 完成！')
        else:
            messagebox.showinfo("epub簡轉繁", "轉檔失敗，請檢查編碼格式！")
        #移除工作檔
        shutil.rmtree(SavePath + tmpEpubPath , ignore_errors=True)

    frame0 = tk.Frame(win)
    btnOpen = tk.Button(frame0, text='選擇簡體 epub 檔' ,command=btnOpenClick).grid(row=0,column=0)
    #btnOpen.pack()

    #v.set('文字檔：')
    label1=tk.Label(frame0, width=30, textvariable=txtfn_v).grid(row=0,column=1)
    #label1.pack()
    frame0.pack()

    frame1 = tk.Frame(win  )
    label2=tk.Label(frame1, text='書名：' ,height=2).grid(row=0,column=0)
    #label2.pack()
    edit1=tk.Entry(frame1 ,width=40 ,textvariable=book_v).grid(row=0,column=1)
    #edit1.pack()
    frame1.pack()

    frame2 = tk.Frame(win )
    label3=tk.Label(frame2, text='作者：' ,height=2).grid(row=0,column=0)
    #label3.pack()
    edit1=tk.Entry(frame2,width=40 ,textvariable=auth_v).grid(row=0,column=1)
    #edit1.pack()
    frame2.pack()

    button1=tk.Button(win, text='開始轉換...' ,width=10 , command=btnClick )
    button1.pack(side='left')

    btnClose=tk.Button(win, text='結束' ,width=10, comman =win.destroy )
    btnClose.pack(side='right')

    win.mainloop()
