#! /usr/bin/python3
# 多個文字檔 轉換成 epub
# 需要 python3-tk 套件
import sys ,os , zipfile ,shutil ,tempfile ,time
import re
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
#簡繁轉換模組
from opencc import OpenCC


#所在程式目錄
ScriptPath = os.path.dirname(sys.argv[0])
#print(ScriptPath)
#暫存區的
SavePath = tempfile.gettempdir()
#print(SavePath)

def unzip_epub():
    #解壓縮 epub
    with zipfile.ZipFile(ScriptPath + '/epub-src.epub', 'r') as myzip:
        for file in myzip.namelist():
            myzip.extract(file,SavePath + '/epub')


html_beg='''<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-TW" xmlns:xml="http://www.w3.org/XML/1998/namespace">
<head>
  <!-- InstanceBeginEditable name="doctitle" -->

  <title>%s</title>
<!-- InstanceEndEditable -->  <link href="../Styles/Style.css" rel="stylesheet" type="text/css"/>
<!-- InstanceBeginEditable name="head" -->  <!-- InstanceEndEditable -->
</head>

<body>
  <div>
    <div>
      <!-- InstanceBeginEditable name="Content" -->
'''


html_end='''
    </div>
<!--End content-->  </div>
<!--End ADE-->  <!-- InstanceEnd -->
</body>
</html>
'''

navpoint="""
              <navPoint id="navPoint-%d" playOrder="%d">
                <navLabel>
                  <text>%s</text>
                </navLabel>
                <content src="Text/ch%d.html"/>
              </navPoint>
"""

tocdoc="""<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN"
 "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd"><ncx version="2005-1" xmlns="http://www.daisy.org/z3986/2005/ncx/">
  <head>
    <meta content="urn:uuid:eba0e149-7241-43f3-a563-7b132597c3a5" name="dtb:uid"/>
    <meta content="1" name="dtb:depth"/>
    <meta content="0" name="dtb:totalPageCount"/>
    <meta content="0" name="dtb:maxPageNumber"/>
  </head>
  <docTitle>
    <text>%s</text>
  </docTitle>
  <navMap>
    %s
  </navMap>
</ncx>
"""

content_beg="""<?xml version="1.0" encoding="utf-8"?>
<package version="2.0" unique-identifier="BookID" xmlns="http://www.idpf.org/2007/opf">
  <metadata xmlns:opf="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/">
    <dc:title>%s</dc:title>
    <dc:creator opf:role="aut">%s</dc:creator>
    <dc:language>zh</dc:language>
    <dc:identifier opf:scheme="UUID" id="BookID">urn:uuid:8c7b74e5-d8c5-40e1-bf6f-55cbc5db9c1f</dc:identifier>
    <meta content="0.9.9" name="Sigil version" />
    <dc:date opf:event="modification">%s</dc:date>
  </metadata>
  <manifest>
    <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml"/>
    <item id="Style.css" href="Styles/Style.css" media-type="text/css"/>
    %s
  </manifest>
  <spine toc="ncx">
    %s
  </spine>
  <guide>
  </guide>
</package>
"""






#寫入分吤章節 html
def savepage(page,doc):
    fn = SavePath + ('/epub/OEBPS/Text/ch%d.html' % page)
    file= open(fn,'w' ,encoding='utf8')
    file.write( doc + html_end)
    file.close()

def saveToc(navpoint):
    fn = SavePath + '/epub/OEBPS/toc.ncx'
    #print(fn)
    file= open(fn,'w', encoding='utf8')
    file.write(tocdoc % ('book',navpoint) )
    file.close()

def savecontent(book_name, auth_name, page):
    fn = SavePath + '/epub/OEBPS/content.opf'
    #print(fn)
    item=''
    itemref=''

    file= open(fn,'w', encoding='utf8')
    today= time.strftime("%Y-%m-%d", time.localtime())

    for i in range(101,page+1):
        item= item+ '   <item id="ch%d.html" href="Text/ch%d.html" media-type="application/xhtml+xml"/>\n' %(i,i)
        itemref=itemref + ' <itemref idref="ch%s.html"/>\n' % i

    file.write(content_beg % (book_name ,auth_name,today , item , itemref)  )
    file.close()



#讀取文字檔
def mtxt2html(fnz ,book_name , auth_name ,decode ,ts):

    page=101
    line_num=1
    result=True
    html_doc= html_beg % "首頁"

    nav = navpoint %(101,101,'首頁',101)
    #
    openCC = OpenCC()
    if ts == 2:
        #簡轉繁
        openCC.set_conversion('s2twp')
    if ts == 3:
        #繁轉簡
        openCC.set_conversion('tw2s')

    for fn in fnz:

        try:
            #編碼
            if decode==3:
                dc='GBK'
                #file = open(fn,'r').read().decode("GBK").encode("utf8")
                file= open(fn,"rb")
            elif decode==2:
                dc='BIG5'
                #file = open(fn,'r').read().decode("big5").encode("utf8")
                file= open(fn,"rb")
            else:
                dc=''
                file= open(fn,"r",encoding='utf8')

            #取得檔名
            tmpepub_fn = os.path.split(fn)[1]
            #title、章節選單
            html_doc= html_beg % tmpepub_fn.split('.')[0]
            nav = nav + navpoint % (page, page, tmpepub_fn.split('.')[0] , page)
            for line in file.readlines():
                if dc != '' :
                    line=str(line.decode(dc) )

                if ts>1:
                    line = openCC.convert(line)

                line = line.strip()  #移除頭尾特定字符如空白
                html_doc= html_doc + '<p>' +line  +'</p>\n'
                result = True
        except  Exception as e:
            print(tmpepub_fn + ':' + str(e))
            result=False
        finally:
            file.close()

        if result:
            savepage(page , html_doc)
            page = page +1
        else:
            return result

    #章節選單
    if nav:
        saveToc(nav)
    #content
    savecontent(book_name, auth_name, page)

    return result

if __name__ == "__main__":

    win=tk.Tk()
    win.title("多文字檔 to epub")
    txtfn=''
    book=''
    auth=''

    filez = []

    txtfn_v = tk.StringVar()
    txtfn_v.set('')

    book_v = tk.StringVar()
    book_v.set('')

    auth_v = tk.StringVar()
    auth_v.set('')

    txt_code = tk.IntVar()
    txt_code.set(1)

    ts_mode = tk.IntVar()
    ts_mode.set(1)

    FILEOPENOPTIONS = dict(defaultextension='.txt',
                       filetypes=[('文字檔','*.txt'), ('所有檔案','*.*')])

    def btnOpenClick():
        global txtfn ,filez

        filez = filedialog.askopenfilenames(**FILEOPENOPTIONS)
        txtfn= filez[0]
        #txtfn_v.set(txtfn)
        #tmpepub_fn = os.path.split(txtfn)[1]
        #book_v.set(tmpepub_fn.split('.')[0] )

    def btnClick():
        global txtfn
        book= book_v.get()
        auth= auth_v.get()
        decode= txt_code.get()
        ts=ts_mode.get()

        #epub 架構檔
        unzip_epub()
        #開始轉換
        success = mtxt2html( filez ,book,auth ,decode , ts)

        if success :

            #壓縮 成 epub，存在同目錄
            newpubfn=book +'_'+ auth +'.epub'
            zf = zipfile.ZipFile(os.path.dirname(txtfn) + '/'+newpubfn , mode='w')#只儲存不壓縮
            os.chdir(SavePath +'/epub')
            for root, folders, files in os.walk("./"):
                for sfile in files:
                    aFile = os.path.join(root, sfile)
                    zf.write(aFile)
            zf.close()
            #label1.text=''
            txtfn=''
            book=''
            auth=''
            txtfn_v.set('')
            book_v.set('')
            auth_v.set('')

            #message
            messagebox.showinfo("txt2epub", "文字檔轉成 " +  newpubfn  +' 完成！')
        else:
            messagebox.showinfo("txt2epub", "轉檔失敗，請檢查編碼格式！")
        #移除工作檔
        shutil.rmtree(SavePath +'/epub' , ignore_errors=True)

    # tkinter 元件
    frame0 = tk.Frame(win)
    btnOpen = tk.Button(frame0, text='選擇多個文字檔' ,command=btnOpenClick).grid(row=0,column=0)
    #btnOpen.pack()

    #v.set('文字檔：')
    label1=tk.Label(frame0, width=30, textvariable=txtfn_v).grid(row=0,column=1)
    #label1.pack()
    frame0.pack()



    fram_select = tk.Frame(win  )
    tk.Label(fram_select, text="文字檔編碼：").grid(row=0,column=0)
    tk.Radiobutton(fram_select,
              text="UTF-8",
              padx = 20,
              variable=txt_code,
              value=1).grid(row=0,column=1)
    tk.Radiobutton(fram_select,
              text="BIG5",
              padx = 20,
              variable=txt_code,
              value=2).grid(row=0,column=2)
    tk.Radiobutton(fram_select,
              text="GBK",
              padx = 20,
              variable=txt_code,
              value=3).grid(row=0,column=3)
    fram_select.pack()

    frame_ts = tk.Frame(win  )
    tk.Label(frame_ts, text="簡繁轉換").grid(row=0,column=0)
    tk.Radiobutton(frame_ts,
              text="不轉換",
              padx = 20,
              variable=ts_mode,
              value=1).grid(row=0,column=1)
    tk.Radiobutton(frame_ts,
              text="簡轉繁Tw",
              padx = 20,
              variable=ts_mode,
              value=2).grid(row=0,column=2)
    tk.Radiobutton(frame_ts,
              text="繁轉簡",
              padx = 20,
              variable=ts_mode,
              value=3).grid(row=0,column=3)
    frame_ts.pack()

    frame1 = tk.Frame(win  )
    label2=tk.Label(frame1, text='書名：' ,height=2).grid(row=0,column=0)
    #label2.pack()
    edit1=tk.Entry(frame1 ,width=40 ,textvariable=book_v).grid(row=0,column=1)
    #edit1.pack()
    frame1.pack()

    frame2 = tk.Frame(win )
    label3=tk.Label(frame2, text='作者：' ,height=2).grid(row=0,column=0)
    #label3.pack()
    edit1=tk.Entry(frame2,width=40 ,textvariable=auth_v).grid(row=0,column=1)
    #edit1.pack()
    frame2.pack()

    button1=tk.Button(win, text='開始轉換...' ,width=10 , command=btnClick )
    button1.pack(side='left')

    btnClose=tk.Button(win, text='結束' ,width=10, comman =win.destroy )
    btnClose.pack(side='right')

    win.mainloop()
