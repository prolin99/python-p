import tkinter as tk
#下載 mp4 模組
from pytube import YouTube
import os


window = tk.Tk()
# 設定視窗標題、大小和背景顏色
window.title('youtube Download App')
window.geometry('480x200')
#window.configure(background='white')

def yt_download(url , media_type):
    if media_type == "1":
        video = YouTube(url).streams.first()

    elif media_type == "2":
        video = YouTube(url).streams.filter(only_audio = True).first()

    else:
        print("Invalid selection.")
    # 取得的 mp4 可在左方檔案找到
    fn = video.download() 

    # 改名
    if media_type == "2":
        mp3_fn = fn.replace(".mp4" , ".mp3")
        os.rename( fn , mp3_fn)
        fn = mp3_fn
    return fn 

def download_it():
    url = url_entry.get()
    media_type = m_entry.get()
    result_label.configure(text='下載中....')
    fn = yt_download(url , media_type)
    result_label.configure(text='下載完成：'+ fn )


header_label = tk.Label(window, text='下載 youtube 影片')
header_label.pack()

# 以下為 height_frame 群組
height_frame = tk.Frame(window)
# 向上對齊父元件
height_frame.pack(side=tk.TOP)
url_label = tk.Label(height_frame, text='youtube網址：')
url_label.pack(side=tk.LEFT)
url_entry = tk.Entry(height_frame)
url_entry.pack(side=tk.LEFT)

# 以下為 weight_frame 群組
weight_frame = tk.Frame(window)
weight_frame.pack(side=tk.TOP)
m_label = tk.Label(weight_frame, text='1.mp4 2.mp3：')
m_label.pack(side=tk.LEFT)
m_entry = tk.Entry(weight_frame)
m_entry.pack(side=tk.LEFT)

result_label = tk.Label(window)
result_label.pack()


calculate_btn = tk.Button(window, text='馬上下載', command = download_it,)
calculate_btn.pack()


# 運行主程式
window.mainloop()

