def change_val(x,y):
    x= 'new value'
    y=y+1
    return x, y

x = 'old value'
y=5
print("x =%s , y=%s" % (x,y)  )

#改變傳入值
x , y = change_val(x,y)
print("x =%s , y=%s" % (x,y)  )

def list_add(list_name):
    list_name.append('marry')

list_A=['tom', 'jhon']
print(list_A)

list_add(list_A)
print(list_A)

list_B=['paul', 'kathy']
print(list_B)

list_add(list_B[:])
print(list_B)
