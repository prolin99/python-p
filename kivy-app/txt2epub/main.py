from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout

from kivy.core.text import LabelBase, DEFAULT_FONT
from kivy.resources import resource_add_path

#改換預設定型，要完整路徑 ，如果要跨平台可能要自附字型檔
#resource_add_path('/system/fonts/')
#LabelBase.register(DEFAULT_FONT, 'DroidSansFallback.ttf')

resource_add_path('/usr/share/fonts/opentype/noto')
LabelBase.register(DEFAULT_FONT, 'NotoSansCJK-Regular.ttc')

from kivy.lang import Builder
from kivy.properties import ObjectProperty , ListProperty ,StringProperty ,NumericProperty
from kivy.uix.popup import Popup
from kivy.uix.label import Label

import sys ,os
import t2epub

#視窗大小
from kivy.core.window import Window
Window.size = (480, 600)

class Text2epubLayout(BoxLayout):
    #要和 kv 介面互動，要使用 Property
    vh_set = ObjectProperty()
    tw_set = ObjectProperty()
    bookname = ObjectProperty()
    author = ObjectProperty()

    # Kv 介面可以存取，但修改值無法更改所在介面內容
    code_def = NumericProperty(0)
    txtfn = StringProperty('')
    tmpepub_fn = StringProperty('')
    txtpath=''


    def select_fn(self, popupForm , path,filename):
        #filename 是完整路徑檔案
        print('file name {}  ---  {}'.format(path,filename ) )
        #關掉 popup
        popupForm.dismiss()

        self.txtfn =filename[0]
        self.txtpath = path

        self.tmpepub_fn = os.path.split(self.txtfn)[1]
        self.tmpepub_fn =self.tmpepub_fn[:-4]



    def toepub(self):
        ts=0
        VH=0

        #開始轉換，取得 直向、簡轉繁
        #print("code {}".format( self.code_def )  )
        #print("code {}".format( self.code_set.data )  )
        #取得 特定的子項 ids 名
        VH = self.vh_set.ids.cb.active
        #print("直向文字 {}".format(self.vh_set.ids.cb.active))
        #print(self.vh_set.children[1].active)
        if (self.tw_set.ids.cb.active):
            ts =2
        #ts = self.tw_set.ids.cb.active
        #print("簡轉繁 {}".format(self.tw_set.ids.cb.active))
        #print(self.tw_set.children[1].active)
        #print("作者 {}".format(self.author.text) )
        book_name = self.bookname.text
        auth_name =self.author.text
        #epub 架構檔
        t2epub.unzip_epub()
        #開始轉換
        success = t2epub.txt2html(self.txtfn ,book_name , auth_name  ,ts ,VH , self.code_def )

        #彃出視窗
        if success:
            the_content = Label(text = "文字檔轉換 Epub 成功！")
            the_content.color = (1,1,1,1)
        else :
            the_content = Label(text = "文字檔轉換 Epub 失敗！")
            the_content.color = (1,0,0,1)
        popup = Popup(title='text 2 epub', content = the_content, size_hint=(None, None),size=(350, 150))
        popup.open()


class T2epubApp(App):
    #pass
    def build(self):
        return Text2epubLayout()

if __name__ == '__main__':

    tpub = T2epubApp()
    tpub.run()
