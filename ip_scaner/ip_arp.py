#!/usr/bin/env python3
#使用 arp 方式取得 ip 和 mac

from scapy.all import *
import sys,getopt


def usage():
        print ("Usage: sudo ./ip_arp.py ")

def main(argv):
        try:
                opts, args = getopt.getopt(argv, "")
        except getopt.GetoptError:
                usage()
                sys.exit(2)
        #response = os.system("ping -c 1 -b " + '120.116.25.255' + " > /dev/null ")
        for ipFix in range(1,254):
                ip = "120.116.24."+str(ipFix)
                arpPkt = Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip, hwdst="ff:ff:ff:ff:ff:ff")
                res = srp1(arpPkt, timeout=1, verbose=0)
                if res:
                        print ("IP= " + res.psrc + "    , MAC= " + res.hwsrc )

        #response = os.system("ping -c 1 -b " + '120.116.25.255' + " > /dev/null ")
        for ipFix in range(1,254):
                ip = "120.116.25."+str(ipFix)
                arpPkt = Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip, hwdst="ff:ff:ff:ff:ff:ff")
                res = srp1(arpPkt, timeout=1, verbose=0)
                if res:
                        print ("IP= " + res.psrc + "    , MAC= " + res.hwsrc )
if __name__ == "__main__":
        main(sys.argv[1:])
