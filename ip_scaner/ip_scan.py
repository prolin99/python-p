#掃 IP  port 號，有連線則再取 arp 取 mac
#需要 root 權限執行  sudo python3 ip_scan.py
#pip3 install  scapy

import socket
import threading
import sys
from scapy.all import *

#ip0=input('STARTING IP  : ')
#ip1=input('ENDING IP    : ')
#port=int(input('PORT NUMBER  : '))
# ip 範圍
ip0='120.116.24.0'
ip1='120.116.25.255'

#偵測 port
port_list=[139,80,443,8080,7,5200,9100]
#timeout=int(input('TIMEOUT      :'))

#等待時間，可微調
timeout=10
timeout=timeout/1000

ip0=list(map(int,ip0.split(".")))
ip01=list(map(int,ip1.split(".")))
class IpAddr:
    def __init__(self,d,c,b,a):
        self.a=a
        self.b=b
        self.c=c
        self.d=d
    def increase(self):
        self.a+=1
        if self.a>255:
            self.a=0
            self.b+=1
        if self.b>255:
            self.b=0
            self.c+=1
        if self.c>255:
            self.c=0
            self.d+=1
        return str(self.d)+"."+str(self.c)+"."+str(self.b)+"."+str(self.a)
    def data(self):
        return str(self.d)+"."+str(self.c)+"."+str(self.b)+"."+str(self.a)
    def next(self):
        self.b+=1
        if self.b>255:
            self.b=0
            self.c+=1
        if self.c>255:
            self.c=0
            self.d+=1
        return str(self.d)+"."+str(self.c)+"."+str(self.b)+"."+str(self.a)
    def diff(self,x,y,z,w):
        return (x-self.d)*256**2+(y-self.c)*256+z-self.b



def find(z,i   ):
    i=list(map(int,i.split(".")))
    ip=IpAddr(i[0],i[1],i[2],i[3])
    global cnt
    while i!=z:
        i=ip.data()
        gotit= False


        for port_l in port_list:
            #print(i , port_l)
            s=socket.socket()
            s.settimeout(timeout)
            try:
                s.connect((i,port_l))
                #print('[*] ACTIVE HOST: ', i , ' :' , port_l    )
                cnt+=1
                gotit= True
            except:
                pass
            s.close()
            if gotit:
                break

        if not gotit:
            #ping test
            response = os.system("ping -c 1 -W 0.3 " + i + " > /dev/null ")
            if response == 0:
                gotit= True
                print('ping ' , i )


        #測試存活
        if  gotit:
            #再由 apr 取 mac
            nip = i
            arpPkt = Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=nip, hwdst="ff:ff:ff:ff:ff:ff")
            res = srp1(arpPkt, timeout=1, verbose=0)
            if res:
                    print ("IP= " + res.psrc + "     MAC= " + res.hwsrc )

        i=ip.increase()


ip=IpAddr(ip0[0],ip0[1],ip0[2],ip0[3])
n=ip.diff(ip01[0],ip01[1],ip01[2],ip01[3])-1
cnt=0
d={}
for i in range(n):
    p,q=ip.data(),ip.next()
    d[i]=threading.Thread(target=lambda:find(q,p))
    d[i].daemon=True
    d[i].start()


find(ip1,ip.data()  )


print('on %s' % cnt)
