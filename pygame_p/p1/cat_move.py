import pygame , sys
from pygame.locals import *
import time

pygame.init()

FPS = 15
fpsClock = pygame.time.Clock()

DISP = pygame.display.set_mode((400,300),0,32)
pygame.display.set_caption('Animation')

WHITE = (255,255,255)
GREEN= (0,255,0)
BLUE = (0,0,128)

catImg = pygame.image.load('cat.png')
catx =10
caty=10
direction ='right'

#文字
fontObj = pygame.font.Font('C:/Windows/Fonts/Arial.ttf',32)
textObj = fontObj.render('heoow', True , GREEN , BLUE )
textRectObj = textObj.get_rect()
textRectObj.center =(200,150)


#soundObj = pygame.mixer.Sound('Alarm01.wav')
#soundObj.play()
#time.sleep(1)
#soundObj.stop()

pygame.mixer.music.load('Alarm01.wav')
pygame.mixer.music.play(-1,0.0)



while True:
    DISP.fill(WHITE)
    DISP.blit(textObj,textRectObj)
    if direction =='right':
        catx += 5
        if catx>=280:
            direction = 'down'
    elif direction == 'down':
        caty += 5
        if caty >= 220:
            direction = 'left'

    elif direction == 'left':
        catx -= 5
        if catx<=10:
            direction = 'up'
    elif direction == 'up' :
        caty -= 5
        if caty<=10:
            direction = 'right'

    DISP.blit(catImg, (catx,caty))

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    #畫面顯示
    pygame.display.update()
    #soundObj.play()
    #每秒觸動次數
    fpsClock.tick(FPS)

pygame.mixer.music.stop()
