import sys

import pygame
from bullet import Bullet

def ckeck_keydown_events(event, ai_settings,screen ,ship , bullets):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    if event.key == pygame.K_LEFT:
        ship.moving_left = True
    if event.key == pygame.K_SPACE:
        fire_bullet(ai_settings, screen ,ship , bullets)


def ckeck_keyup_events(event, ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    if event.key == pygame.K_LEFT:
        ship.moving_left = False

def check_events(ai_settings,screen ,ship, bullets):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            ckeck_keydown_events(event,ai_settings,screen , ship , bullets)
        elif event.type == pygame.KEYUP:
            ckeck_keyup_events(event , ship)

def update_screen(ai_settings, screen , ship , bullets):
        screen.fill(ai_settings.bg_color)
        ship.blitme()
        #每次都重繪
        for bullet in bullets.sprites():
            bullet.draw_bullet()
        pygame.display.flip()

def update_bullets(bullets):
    bullets.update()
    #子彈消失
    for bullet in bullets.copy():
        if bullet.rect.bottom <=0:
            bullets.remove(bullet)

def fire_bullet(ai_settings, screen , ship , bullets):
    if len(bullets) < ai_settings.bullets_allowed:
        new_bullet = Bullet(ai_settings, screen, ship)
        bullets.add(new_bullet)
